from ctypes import WinDLL
from pathlib import Path

# Support rapid iteration by including local diabutil
import sys

sys.path.insert(0, str(Path(__file__).resolve().parent.parent.parent / "src"))
from diabutil.v2 import cel, gif, mpq, pal
from diabutil.v2.types import make_animation

stormlib_path = (
    Path(__file__).resolve().parent.parent.parent
    / "prebuilts"
    / "StormLib-Win64"
    / "StormLib.dll"
)
stormlib = WinDLL(str(stormlib_path))
with mpq.open_archive(stormlib, "DIABDAT.MPQ") as archive:
    palette = pal.decode_pal(archive.read_file("Levels\\L1Data\\L1_1.PAL"))
    cel_rle_frames = cel.decode_cel(archive.read_file("Data\\PentSpin.CEL"))[0]

images = [cel.render_rle(rle, width=48) for rle in cel_rle_frames]
animation = make_animation(images)

# Save the animation as gif
animation, palette = gif.compress_palette(animation, palette)
animation, palette, transparency = gif.resolve_transparency(animation, palette)
gif.save_gif(animation, palette, transparency, gif.DIABLO_DELAY, Path("PentSpin.gif"))
