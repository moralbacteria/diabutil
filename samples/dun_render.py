import diabutil
import diabutil.min
from diabutil import dun, pal, til, PseudoColorAnimation
from diabutil.types import (
    CelRleSequence,
    PseudoColorSequence,
    TRANSPARENT_PIXEL,
    PseudoColorPixel,
)


_TIL_FLOOR_WIDTH = til.TILE_WIDTH_PX
_TIL_FLOOR_HALF_WIDTH = int(_TIL_FLOOR_WIDTH / 2)
_TIL_FLOOR_HEIGHT = 64
_TIL_FLOOR_HALF_HEIGHT = int(_TIL_FLOOR_HEIGHT / 2)

_BASIS_X = 32
_BASIS_Y = 16

# TODO customizable background tile?
_L1_FLOOR_TILE = 12


class Image:
    """The image is probably upside-down because that's how Windows rolls B)"""

    def __init__(self, width: int, height: int, data: PseudoColorSequence):
        self._width = width
        self._height = height
        self._data = data

    def blit(self, other, x: int, y: int):
        for other_y in range(other.get_height()):
            for other_x in range(other.get_width()):
                self_x = other_x + x
                self_y = other_y + y

                other_color = other.at(other_x, other_y)
                if other_color == TRANSPARENT_PIXEL:
                    continue
                self.put(self_x, self_y, other_color)

    def at(self, x: int, y: int) -> PseudoColorPixel:
        return self._data[y * self._width + x]

    def put(self, x: int, y: int, color: PseudoColorPixel):
        self._data[y * self._width + x] = color

    def get_width(self) -> int:
        return self._width

    def get_height(self) -> int:
        return self._height

    def get_data(self) -> PseudoColorSequence:
        return self._data

    @classmethod
    def blank(cls, width: int, height: int):
        return cls(width, height, [TRANSPARENT_PIXEL] * (width * height))


def _get_special(mt: int) -> int:
    """From ObjL1Special"""
    map = {
        12: 1,
        11: 2,
        71: 1,
        259: 5,
        249: 2,
        325: 2,
        321: 1,
        255: 4,
        211: 1,
        344: 2,
        341: 1,
        331: 2,
        418: 1,
        421: 2,
    }
    return 0 if mt not in map else map[mt]


def dun_render(
    all_micros: diabutil.min.Micros,
    all_tiles: list[til.Tile],
    palette: pal.Palette,
    ldata_cel: list[CelRleSequence],
    ldata_s_cel: list[PseudoColorSequence],
    dun_file: dun.DunFile,
) -> PseudoColorAnimation:
    # Convert tiles into micros so we can place specials
    dun_micros_width = dun_file.width * 2
    dun_micros_height = dun_file.height * 2
    dun_micros = [0] * dun_micros_width * dun_micros_height
    for y in range(dun_file.height):
        for x in range(dun_file.width):
            min_index = y * 2 * dun_micros_width + x * 2

            tile_index = y * dun_file.width + x
            tile_data = dun_file.tiles[tile_index]
            if tile_data == 0:
                tile_data = _L1_FLOOR_TILE
            else:
                tile_data -= 1

            tile = all_tiles[tile_data]

            dun_micros[min_index] = tile[0]
            dun_micros[min_index + 1] = tile[1]
            dun_micros[dun_micros_width + min_index] = tile[2]
            dun_micros[dun_micros_width + min_index + 1] = tile[3]

    # one tile + overlaps
    rendered_row_width = til.TILE_WIDTH_PX + _TIL_FLOOR_HALF_WIDTH * (
        dun_file.width - 1
    )
    rendered_row_height = til.TILE_HEIGHT_PX + _TIL_FLOOR_HALF_HEIGHT * (
        dun_file.width - 1
    )

    # one row + overlaps
    image_width = rendered_row_width + _TIL_FLOOR_HALF_WIDTH * (dun_file.height - 1)
    image_height = rendered_row_height + _TIL_FLOOR_HALF_HEIGHT * (dun_file.height - 1)

    micros_image_cache: dict[int, Image] = {}

    image = Image.blank(image_width, image_height)
    for y in range(dun_micros_height):
        for x in range(dun_micros_width):
            mt = dun_micros[y * dun_micros_width + x]
            special_index = _get_special(mt + 1)  # TODO magic number

            if mt not in micros_image_cache:
                micros_image_cache[mt] = Image(
                    diabutil.min.MICROS_RENDERED_WIDTH,
                    diabutil.min.MICROS_RENDERED_HEIGHT,
                    diabutil.min.render_micros(all_micros[mt], ldata_cel),
                )

            # Draw isometrically TODO use isometric math/change of basis
            blit_x = (
                image.get_width()
                - rendered_row_width
                - y * _BASIS_X
                + x * _BASIS_X
                + 32
            )  # TODO magic number
            start_y = (dun_micros_height - 1) * _BASIS_Y + (
                dun_micros_width - 1
            ) * _BASIS_Y
            blit_y = start_y - y * _BASIS_Y - x * _BASIS_Y

            image.blit(micros_image_cache[mt], blit_x, blit_y)

            if special_index:
                special_image = Image(
                    diabutil.min.MICROS_RENDERED_WIDTH,
                    diabutil.min.MICROS_RENDERED_HEIGHT,
                    ldata_s_cel[special_index - 1],
                )
                image.blit(special_image, blit_x, blit_y)

    # TODO objects and enemies

    animation = PseudoColorAnimation(
        image.get_width(), image.get_height(), [image.get_data()], palette
    )
    animation.flip_vertically()

    return animation
