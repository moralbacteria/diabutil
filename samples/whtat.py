"""Convert whtat.cel from Beta into whtat.gif"""

from ctypes import WinDLL
from pathlib import Path

from diabutil import cel, gif, mpq, pal, PseudoColorAnimation

stormlib_path = (
    Path(__file__).resolve().parent.parent
    / "prebuilts"
    / "StormLib-Win64"
    / "StormLib.dll"
)
stormlib = WinDLL(str(stormlib_path))
with mpq.open_archive(stormlib, "DIABDAT.MPQ") as archive:
    # Read and decode whtat.cel into memory
    whtat = cel.deserialize_with_groups(
        archive.read_file("plrgfx\\warrior\\wht\\whtat.cel"),
        pal.decode_pal(archive.read_file("Levels\\L1Data\\L1_1.PAL")),
        width=128,
        num_groups=8,
        has_skip_header=True,
    )

# Flatten the groups; play all directions one after the other
whtat_frames = [frame for anim in whtat for frame in anim.frames]
flat_whtat = PseudoColorAnimation(
    whtat[0].width, whtat[0].height, whtat_frames, whtat[0].palette
)

# Save the animation as gif
flat_whtat.compress_palette()
transparency = gif.resolve_transparency(flat_whtat)
gif.save_gif(flat_whtat, transparency, gif.DIABLO_DELAY, Path("whtat.gif"))
