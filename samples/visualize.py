import argparse
import itertools
import re
from ctypes import WinDLL
from dataclasses import dataclass
from pathlib import Path

from dun_render import dun_render, Image

import diabutil
from diabutil import cel, dun, gif, mpq, pal, pnm, til, PseudoColorAnimation

#
# Helpers
#


def _fix_wludt_header(data: bytes) -> bytes:
    # There's no pattern to fix the header so just hardcode what we expect.
    # This was created via manual inspection in hexed.it
    new_header = (
        b"\x20\x00\x00\x00"
        + b"\xd9\x72\x00\x00"
        + b"\x0f\xee\x00\x00"
        + b"\xb8\x5f\x01\x00"
        + b"\x31\xc8\x01\x00"
        + b"\x09\x2d\x02\x00"
        + b"\x5d\xa0\x02\x00"
        + b"\x98\x12\x03\x00"
    )
    return new_header + data[len(new_header) :]


def _flatten(lst):
    return list(itertools.chain(*lst))


#
# Visualizers
#


@dataclass
class CelVisualizer:
    """Convert a CEL animation (e.g. BloodFnt.cel) into an animated GIF"""

    # TODO: There's a lot of code duplication between the various CEL visualizers. DRY!
    width: int
    palette_entry: bytes = "Levels\\L1Data\\L1_1.pal"
    has_skip_header: bool = False
    gif_delay: int = gif.DIABLO_DELAY

    def prepare(self, archive: mpq.MpqArchive):
        self._palette = pal.decode_pal(archive.read_file(self.palette_entry))

    def visualize(self, data: bytes, out_path: Path):
        anim = cel.deserialize(data, self._palette, self.width, self.has_skip_header)

        # Save the animation as gif
        anim.compress_palette()
        transparency = gif.resolve_transparency(anim)
        out_path = out_path.parent / (out_path.name + ".gif")
        gif.save_gif(anim, transparency, self.gif_delay, out_path)


@dataclass
class GroupedCelVisualizer:
    """Convert a CEL with groups (e.g. PlrGFX) into an animated GIF"""

    width: int
    groups: int
    palette_entry: bytes = "Levels\\L1Data\\L1_1.pal"
    has_skip_header: bool = False
    gif_delay: int = gif.DIABLO_DELAY

    def prepare(self, archive: mpq.MpqArchive):
        self._palette = pal.decode_pal(archive.read_file(self.palette_entry))

    def visualize(self, data: bytes, out_path: Path):
        animations = cel.deserialize_with_groups(
            data, self._palette, self.width, self.groups, self.has_skip_header
        )

        # # Decompose into one animation sequence
        flat_frames = [x for animation in animations for x in animation.frames]
        animation = PseudoColorAnimation(
            animations[0].width,
            animations[0].height,
            flat_frames,
            animations[0].palette,
        )

        # Save the animation as gif
        animation.compress_palette()
        transparency = gif.resolve_transparency(animation)
        out_path = out_path.parent / (out_path.name + ".gif")
        gif.save_gif(animation, transparency, self.gif_delay, out_path)


@dataclass
class WludtVisualizer(GroupedCelVisualizer):
    """Fix and convert WLUDT.CEL into an animated GIF."""

    def visualize(self, data: bytes, out_path: Path):
        super().visualize(_fix_wludt_header(data), out_path)


@dataclass
class CelSingleImageVisualizer:
    """Draw the first CEL frame as PPM"""

    width: int
    palette_entry: bytes = "Levels\\L1Data\\L1_1.pal"
    has_skip_header: bool = False

    def prepare(self, archive: mpq.MpqArchive):
        self._palette = pal.decode_pal(archive.read_file(self.palette_entry))

    def visualize(self, data: bytes, out_path: Path):
        anim = cel.deserialize(data, self._palette, self.width, self.has_skip_header)

        ppm_data = bytes(
            [x for pixel in anim.frames[0] for x in anim.palette[pixel].encode()]
        )
        out_path = out_path.parent / (out_path.name + ".ppm")
        pnm.save_ppm_p6(anim.width, anim.height, ppm_data, out_path)
        # TODO: support gif with transparency saving


@dataclass
class CelSplitVisualizer:
    """Convert each CEL frame into a separate GIF. Supports per-frame widths."""

    widths: list[int]
    palette_entry: bytes = "Levels\\L1Data\\L1_1.pal"
    has_skip_header: bool = False

    def prepare(self, archive: mpq.MpqArchive):
        self._palette = pal.decode_pal(archive.read_file(self.palette_entry))

    def visualize(self, data: bytes, out_path: Path):
        frames = cel.decode_frame_data(data, self.has_skip_header)

        for i in range(len(frames)):
            frame_data = frames[i]
            width = self.widths[i]
            height = int(len(frame_data) / width)
            frame = PseudoColorAnimation(width, height, [frame_data], self._palette)
            frame.flip_vertically()

            frame.compress_palette()
            transparency = gif.resolve_transparency(frame)
            new_out_path = out_path.parent / (out_path.name + f"_{i}.gif")
            gif.save_gif(frame, transparency, 0, new_out_path)


@dataclass
class CelSpriteSheetVisualizer:
    """Convert a CEL into a vertical spreadsheet GIF."""

    width: int
    palette_entry: bytes = "Levels\\L1Data\\L1_1.pal"
    has_skip_header: bool = False

    def prepare(self, archive: mpq.MpqArchive):
        self._palette = pal.decode_pal(archive.read_file(self.palette_entry))

    def visualize(self, data: bytes, out_path: Path):
        anim = cel.deserialize(data, self._palette, self.width, self.has_skip_header)

        # concat all frame data vertically
        spritesheet = PseudoColorAnimation(
            anim.width,
            anim.height * len(anim.frames),
            [_flatten(anim.frames)],
            anim.palette,
        )

        spritesheet.compress_palette()
        transparency = gif.resolve_transparency(spritesheet)
        out_path = out_path.parent / (out_path.name + ".gif")
        gif.save_gif(spritesheet, transparency, 0, out_path)


@dataclass
class CelSpriteSheetHorizontalVisualizer:
    """Convert a CEL into a horizontal spreadsheet GIF."""

    width: int
    palette_entry: bytes = "Levels\\L1Data\\L1_1.pal"
    has_skip_header: bool = False

    def prepare(self, archive: mpq.MpqArchive):
        self._palette = pal.decode_pal(archive.read_file(self.palette_entry))

    def visualize(self, data: bytes, out_path: Path):
        anim = cel.deserialize(data, self._palette, self.width, self.has_skip_header)

        # concat all frame data vertically
        spritesheet_data = []
        for y in range(anim.height):
            for frame in anim.frames:
                index = y * anim.width
                spritesheet_data += frame[index : index + anim.width]

        spritesheet = PseudoColorAnimation(
            anim.width * len(anim.frames),
            anim.height,
            [spritesheet_data],
            anim.palette,
        )

        spritesheet.compress_palette()
        transparency = gif.resolve_transparency(spritesheet)
        out_path = out_path.parent / (out_path.name + ".gif")
        gif.save_gif(spritesheet, transparency, 0, out_path)


class PalVisualizer:
    """Create a 16x16 PPM where each pixel is the color of a palette entry."""

    def prepare(self, archive: mpq.MpqArchive):
        pass

    def visualize(self, data: bytes, out_path: Path):
        out_path = out_path.parent / (out_path.name + ".ppm")
        pnm.save_ppm_p6(16, 16, data, out_path)


@dataclass
class DunVisualizer:
    """Render a .DUN file to PPM"""

    palette_entry: bytes = "Levels\\L1Data\\L1_1.PAL"

    def prepare(self, archive: mpq.MpqArchive):
        self._palette = pal.decode_pal(archive.read_file(self.palette_entry))
        self._micros = diabutil.min.decode_min(
            archive.read_file("Levels\\L1Data\\L1.MIN")
        )
        self._ldata_cel = cel.find_frame_data(
            archive.read_file("Levels\\L1Data\\L1.CEL")
        )
        self._ldata_s_cel = cel.decode_frame_data(
            archive.read_file("Levels\\L1Data\\L1S.CEL"), has_skip_header=True
        )
        self._tiles = til.decode_til(archive.read_file("Levels\\L1Data\\L1.TIL"))

    def visualize(self, data: bytes, out_path: Path):
        animation = dun_render(
            self._micros,
            self._tiles,
            self._palette,
            self._ldata_cel,
            self._ldata_s_cel,
            dun.decode_dun(data),
        )

        ppm_data = bytes(
            [
                x
                for pixel in animation.frames[0]
                for x in animation.palette[pixel].encode()
            ]
        )
        out_path = out_path.parent / (out_path.name + ".ppm")
        pnm.save_ppm_p6(animation.width, animation.height, ppm_data, out_path)


@dataclass
class MinVisualizer:
    """Render a .MIN file as a spritesheet"""

    palette_entry: bytes = "Levels\\L1Data\\L1_1.PAL"

    def prepare(self, archive: mpq.MpqArchive):
        self._palette = pal.decode_pal(archive.read_file(self.palette_entry))
        self._ldata_cel = cel.find_frame_data(
            archive.read_file("Levels\\L1Data\\L1.CEL")
        )

    def visualize(self, data: bytes, out_path: Path):
        # Load the .MIN file and group the mins together
        all_micros = diabutil.min.decode_min(data)
        image_builder = []
        for micros in all_micros:
            anim = PseudoColorAnimation(
                diabutil.min.MICROS_RENDERED_WIDTH,
                diabutil.min.MICROS_RENDERED_HEIGHT,
                [diabutil.min.render_micros(micros, self._ldata_cel)],
                self._palette,
            )
            anim.flip_vertically()
            image_builder += anim.frames[0]

        ppm_data = bytes(
            [x for pixel in image_builder for x in self._palette[pixel].encode()]
        )
        out_path = out_path.parent / (out_path.name + ".ppm")
        pnm.save_ppm_p6(
            diabutil.min.MICROS_RENDERED_WIDTH,
            diabutil.min.MICROS_RENDERED_HEIGHT * len(all_micros),
            ppm_data,
            out_path,
        )


@dataclass
class TilVisualizer:
    """Render a .TIL file as a spritesheet"""

    palette_entry: bytes = "Levels\\L1Data\\L1_1.PAL"

    def prepare(self, archive: mpq.MpqArchive):
        self._palette = pal.decode_pal(archive.read_file(self.palette_entry))
        self._micros = diabutil.min.decode_min(
            archive.read_file("Levels\\L1Data\\L1.MIN")
        )
        self._ldata_cel = cel.find_frame_data(
            archive.read_file("Levels\\L1Data\\L1.CEL")
        )

    def visualize(self, data: bytes, out_path: Path):
        tiles = til.decode_til(data)
        image_builder = []
        for tile in tiles:
            top = Image(
                diabutil.min.MICROS_RENDERED_WIDTH,
                diabutil.min.MICROS_RENDERED_HEIGHT,
                diabutil.min.render_micros(self._micros[tile[0]], self._ldata_cel),
            )
            right = Image(
                diabutil.min.MICROS_RENDERED_WIDTH,
                diabutil.min.MICROS_RENDERED_HEIGHT,
                diabutil.min.render_micros(self._micros[tile[1]], self._ldata_cel),
            )
            left = Image(
                diabutil.min.MICROS_RENDERED_WIDTH,
                diabutil.min.MICROS_RENDERED_HEIGHT,
                diabutil.min.render_micros(self._micros[tile[2]], self._ldata_cel),
            )
            bottom = Image(
                diabutil.min.MICROS_RENDERED_WIDTH,
                diabutil.min.MICROS_RENDERED_HEIGHT,
                diabutil.min.render_micros(self._micros[tile[3]], self._ldata_cel),
            )

            image = Image.blank(til.TILE_WIDTH_PX, til.TILE_HEIGHT_PX)
            # TODO magic numbers
            image.blit(bottom, 32, 0)
            image.blit(right, 64, 16)
            image.blit(left, 0, 16)
            image.blit(top, 32, 32)

            anim = PseudoColorAnimation(
                image.get_width(), image.get_height(), [image.get_data()], self._palette
            )
            anim.flip_vertically()
            image_builder += anim.frames[0]

        ppm_data = bytes(
            [x for pixel in image_builder for x in self._palette[pixel].encode()]
        )
        out_path = out_path.parent / (out_path.name + ".ppm")
        pnm.save_ppm_p6(
            til.TILE_WIDTH_PX, til.TILE_HEIGHT_PX * len(tiles), ppm_data, out_path
        )


#
# Map of file -> visualizer
#

_METADATA = {
    "CtrlPan/P7Bulbs.CEL": CelSpriteSheetVisualizer(width=88),
    "CtrlPan/PanBtns.CEL": CelSplitVisualizer(widths=[52] * 2 + [27] * 4),
    "CtrlPan/Panel7.CEL": CelVisualizer(width=640),
    "CtrlPan/SmalText.CEL": CelSpriteSheetVisualizer(width=13),
    "CtrlPan/SpelIcon.CEL": CelSpriteSheetVisualizer(width=56),
    "CtrlPan/tbtext.CEL": CelSpriteSheetVisualizer(width=18),  # unused
    "Data/BigTGold.CEL": CelSpriteSheetVisualizer(width=46),
    "Data/Char.CEL": CelSingleImageVisualizer(width=320),
    "Data/CharBut.CEL": CelSplitVisualizer(widths=[95] + [41] * 8),
    "Data/cursors3.CEL": CelSpriteSheetVisualizer(
        width=64, has_skip_header=True
    ),  # unused
    "Data/DiabSmal.CEL": CelVisualizer(width=296),
    "Data/hugetext.CEL": CelSpriteSheetVisualizer(width=46),  # unused
    "Data/MedTextS.CEL": CelSpriteSheetVisualizer(width=22),
    "Data/PentSpin.CEL": CelVisualizer(width=48),
    "Data/PentSpn2.CEL": CelVisualizer(width=12),
    "Data/Square.CEL": CelVisualizer(width=64, has_skip_header=True),
    "Data/TextBox.CEL": CelVisualizer(width=591),
    "Data/TextBox2.CEL": CelVisualizer(width=271),
    "Data/TextSlid.CEL": CelSpriteSheetVisualizer(width=12),
    "Data/Inv/Inv.CEL": CelVisualizer(width=320),
    "Data/Inv/Inv.pal": PalVisualizer(),
    "Data/Inv/Objcurs.CEL": CelSplitVisualizer(
        widths=[33] * 2 + [32] * 5 + [28] * 50 + [56] * 68, has_skip_header=True
    ),
    "Gendata/bigtext.CEL": CelSpriteSheetVisualizer(
        width=22, palette_entry="Gendata\\mainmenu.pal"
    ),  # unused
    "Gendata/blizno.CEL": CelSingleImageVisualizer(
        width=640, palette_entry="Gendata\\blizno.pal"
    ),  # unused
    "Gendata/blizno.pal": PalVisualizer(),  # unused
    "Gendata/bliznort.CEL": CelSingleImageVisualizer(
        width=640, palette_entry="Gendata\\bliznort.pal"
    ),  # unused
    "Gendata/bliznort.pal": PalVisualizer(),  # unused
    "Gendata/charscrn.pal": PalVisualizer(),  # unused
    "Gendata/create.pal": PalVisualizer(),  # unused
    "Gendata/create4.pal": PalVisualizer(),  # unused
    "Gendata/creatnet.pal": PalVisualizer(),  # unused
    "Gendata/creatscr.pal": PalVisualizer(),  # unused
    "Gendata/Cutl1d.CEL": CelSingleImageVisualizer(
        width=640, palette_entry="Gendata\\Cutl1d.pal"
    ),
    "Gendata/Cutl1d.pal": PalVisualizer(),
    "Gendata/Cuttt.CEL": CelSingleImageVisualizer(
        width=640, palette_entry="Gendata\\Cuttt.pal"
    ),
    "Gendata/Cuttt.pal": PalVisualizer(),
    "Gendata/delchar.pal": PalVisualizer(),
    # 'Gendata/Diabfr.CEL': CelVisualizer(width=640, palette_entry='Gendata/Title.pal'), # TODO No space in palette for transparency! uses 233 colors
    # TODO: gendata/diablo.smk
    "Gendata/joinnet.pal": PalVisualizer(),  # unused
    "Gendata/loadchar.pal": PalVisualizer(),  # unused
    # TODO: gendata/logo.smk
    "Gendata/mainmenu.pal": PalVisualizer(),  # unused
    "Gendata/multi.pal": PalVisualizer(),  # unused
    "Gendata/netgames.pal": PalVisualizer(),  # unused
    "Gendata/Pentitle.CEL": CelVisualizer(
        width=48, palette_entry="Gendata\\Title.pal", has_skip_header=False
    ),
    "Gendata/quote.pal": PalVisualizer(),  # unused
    "Gendata/quote1.CEL": CelSingleImageVisualizer(
        width=640, palette_entry="Gendata\\quote.pal"
    ),  # unused
    "Gendata/quote2.CEL": CelSingleImageVisualizer(
        width=640, palette_entry="Gendata\\quote.pal"
    ),  # unused
    "Gendata/Quotes.pal": PalVisualizer(),
    "Gendata/Quotes.CEL": CelSingleImageVisualizer(
        width=640, palette_entry="Gendata\\Quotes.pal"
    ),  # TODO: Replace pal[255] with black for a better render
    "Gendata/Screen01.CEL": CelSingleImageVisualizer(
        width=640, palette_entry="Gendata\\Screen01.pal"
    ),
    "Gendata/Screen01.pal": PalVisualizer(),
    "Gendata/Screen02.CEL": CelSingleImageVisualizer(
        width=640, palette_entry="Gendata\\Screen02.pal"
    ),
    "Gendata/Screen02.pal": PalVisualizer(),
    "Gendata/Screen03.CEL": CelSingleImageVisualizer(
        width=640, palette_entry="Gendata\\Screen03.pal"
    ),
    "Gendata/Screen03.pal": PalVisualizer(),
    "Gendata/Screen04.CEL": CelSingleImageVisualizer(
        width=640, palette_entry="Gendata\\Screen04.pal"
    ),
    "Gendata/Screen04.pal": PalVisualizer(),
    "Gendata/Screen05.CEL": CelSingleImageVisualizer(
        width=640, palette_entry="Gendata\\Screen05.pal"
    ),
    "Gendata/Screen05.pal": PalVisualizer(),
    "Gendata/Screen06.CEL": CelSingleImageVisualizer(
        width=640, palette_entry="Gendata\\Screen06.pal"
    ),  # unused; looks like Screen05.CEL but different SHA...
    "Gendata/Screen06.pal": PalVisualizer(),
    "Gendata/Title.CEL": CelSingleImageVisualizer(
        width=640, palette_entry="Gendata\\Title.pal"
    ),  # unused
    "Gendata/Title.pal": PalVisualizer(),
    "Gendata/Titlgray.CEL": CelSingleImageVisualizer(
        width=640, palette_entry="Gendata\\Title.pal"
    ),
    "Gendata/Titlqtxt.CEL": CelSpriteSheetVisualizer(
        width=22, palette_entry="Gendata\\Title.pal"
    ),
    "Gendata/Titltext.CEL": CelSpriteSheetVisualizer(
        width=46, palette_entry="Gendata\\Title.pal"
    ),
    # TODO: items
    "Levels/L1Data/Hero1.DUN": DunVisualizer(),  # TODO: Overlay this on Hero2.DUN then render
    "Levels/L1Data/Hero2.DUN": DunVisualizer(),
    "Levels/L1Data/L1.MIN": MinVisualizer(),
    "Levels/L1Data/L1.TIL": TilVisualizer(),
    "Levels/L1Data/L1_1.pal": PalVisualizer(),
    "Levels/L1Data/L1_2.pal": PalVisualizer(),
    "Levels/L1Data/L1_3.pal": PalVisualizer(),
    "Levels/L1Data/L1_4.pal": PalVisualizer(),
    "Levels/L1Data/L1_5.pal": PalVisualizer(),
    "Levels/L1Data/L1S.CEL": CelSpriteSheetVisualizer(width=64, has_skip_header=True),
    "Levels/L1Data/l1palg.pal": PalVisualizer(),  # unused
    "Levels/L1Data/Lv1MazeA.DUN": DunVisualizer(
        palette_entry="Levels\\L1Data\\L1_5.PAL"
    ),  # unused; part of LoadSetMap but never loaded
    "Levels/L1Data/Lv1MazeB.DUN": DunVisualizer(
        palette_entry="Levels\\L1Data\\L1_5.PAL"
    ),  # unused; part of LoadSetMap but never loaded
    "Levels/L1Data/rnd1.DUN": DunVisualizer(),  # unused
    "Levels/L1Data/rnd2.DUN": DunVisualizer(),  # unused
    "Levels/L1Data/rnd3.DUN": DunVisualizer(),  # unused
    "Levels/L1Data/rnd4.DUN": DunVisualizer(),  # unused
    "Levels/L1Data/rnd5.DUN": DunVisualizer(),  # "unused"... Play Pre-ablo and turn Alpha DRLG on! ;)
    "Levels/L1Data/rnd6.DUN": DunVisualizer(),
    "Levels/L1Data/sklkng.DUN": DunVisualizer(
        palette_entry="Levels\\L1Data\\L1_2.PAL"
    ),  # unused; palette is a guess
    "Levels/L1Data/SklKng1.DUN": DunVisualizer(
        palette_entry="Levels\\L1Data\\L1_2.PAL"
    ),
    "Levels/L1Data/SklKng2.DUN": DunVisualizer(
        palette_entry="Levels\\L1Data\\L1_2.PAL"
    ),
    "Levels/L1Data/sklkngDR.DUN": DunVisualizer(),  # unused
    "Levels/L1Data/SKngDC.DUN": DunVisualizer(),
    "Levels/L1Data/SKngDO.DUN": DunVisualizer(),
    # TODO: L1.CEL tile sprite sheet?
    # TODO: That one DUN that doesn't have an MPQ name...
    "Levels/TownData/Town.pal": PalVisualizer(),
    "Levels/TownData/TownS.CEL": CelSpriteSheetVisualizer(
        width=64, palette_entry="Levels\\TownData\\Town.pal"
    ),
    "Levels/TownData/ltpalg.pal": PalVisualizer(),  # unused
    # TODO Levels/TownData/*
    "Missiles/Arrow.CEL": CelSpriteSheetVisualizer(
        width=96, has_skip_header=True
    ),  # unused
    "Missiles/Arrows.CEL": CelSpriteSheetVisualizer(
        width=96, has_skip_header=True
    ),  # Bow arrow
    "Missiles/Bigexp.CEL": CelVisualizer(width=160, has_skip_header=True),  # Fireball
    "Missiles/Blodbur0.CEL": CelVisualizer(
        width=128, has_skip_header=True, gif_delay=gif.DIABLO_DELAY * 2
    ),  # Blood Boil (enemy location)
    "Missiles/Blodburs.CEL": CelVisualizer(
        width=128, has_skip_header=True, gif_delay=gif.DIABLO_DELAY * 2
    ),  # Blood Boil (player location, and on floor)
    "Missiles/Blood1.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # TODO this is associated with a missile but is the missile used?
    "Missiles/Blood2.CEL": CelVisualizer(
        width=128, has_skip_header=True
    ),  # same as Missiles/Blodburs.CEL; TODO this is associated with a missile but is the missile used?
    "Missiles/Blood3.CEL": CelVisualizer(
        width=128, has_skip_header=True
    ),  # same as Missiles/Blodbur0.CEL; TODO this is associated with a missile but is the missile used?
    "Missiles/Blood4.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # unused; same as Missiles/Blood1.CEL
    "Missiles/Bluexbk.CEL": CelVisualizer(width=160, has_skip_header=True),  # Flash
    "Missiles/Bluexfr.CEL": CelVisualizer(width=160, has_skip_header=True),  # Flash
    "Missiles/Bone1.CEL": CelVisualizer(
        width=128, has_skip_header=True, gif_delay=gif.DIABLO_DELAY * 2
    ),  # TODO this is associated with a missile but is the missile used?
    "Missiles/Bone2.CEL": CelVisualizer(
        width=128, has_skip_header=True, gif_delay=gif.DIABLO_DELAY * 2
    ),  # TODO this is associated with a missile but is the missile used?
    "Missiles/Bone3.CEL": CelVisualizer(
        width=128, has_skip_header=True, gif_delay=gif.DIABLO_DELAY * 2
    ),  # TODO this is associated with a missile but is the missile used?
    "Missiles/Doom1.CEL": CelVisualizer(width=96, has_skip_header=True),  # Doom Serpent
    "Missiles/Doom2.CEL": CelVisualizer(width=96, has_skip_header=True),  # Doom Serpent
    "Missiles/Doom3.CEL": CelVisualizer(width=96, has_skip_header=True),  # Doom Serpent
    "Missiles/Doom4.CEL": CelVisualizer(width=96, has_skip_header=True),  # Doom Serpent
    "Missiles/Doom5.CEL": CelVisualizer(width=96, has_skip_header=True),  # Doom Serpent
    "Missiles/Doom6.CEL": CelVisualizer(width=96, has_skip_header=True),  # Doom Serpent
    "Missiles/Doom7.CEL": CelVisualizer(width=96, has_skip_header=True),  # Doom Serpent
    "Missiles/Doom8.CEL": CelVisualizer(width=96, has_skip_header=True),  # Doom Serpent
    "Missiles/Doomexp.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Doom Serpent
    "Missiles/Farrow1.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Fire arrows
    "Missiles/Farrow2.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Fire arrows
    "Missiles/Farrow3.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Fire arrows
    "Missiles/Farrow4.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Fire arrows
    "Missiles/Farrow5.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Fire arrows
    "Missiles/Farrow6.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Fire arrows
    "Missiles/Farrow7.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Fire arrows
    "Missiles/Farrow8.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Fire arrows
    "Missiles/Farrow9.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Fire arrows
    "Missiles/Farrow10.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Fire arrows
    "Missiles/Farrow11.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Fire arrows
    "Missiles/Farrow12.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Fire arrows
    "Missiles/Farrow13.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Fire arrows
    "Missiles/Farrow14.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Fire arrows
    "Missiles/Farrow15.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Fire arrows
    "Missiles/Farrow16.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Fire arrows
    "Missiles/Fireba1.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Firebolt, Fireball
    "Missiles/Fireba2.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Firebolt, Fireball
    "Missiles/Fireba3.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Firebolt, Fireball
    "Missiles/Fireba4.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Firebolt, Fireball
    "Missiles/Fireba5.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Firebolt, Fireball
    "Missiles/Fireba6.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Firebolt, Fireball
    "Missiles/Fireba7.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Firebolt, Fireball
    "Missiles/Fireba8.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Firebolt, Fireball
    "Missiles/Fireba9.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Firebolt, Fireball
    "Missiles/Fireba10.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Firebolt, Fireball
    "Missiles/Fireba11.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Firebolt, Fireball
    "Missiles/Fireba12.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Firebolt, Fireball
    "Missiles/Fireba13.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Firebolt, Fireball
    "Missiles/Fireba14.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Firebolt, Fireball
    "Missiles/Fireba15.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Firebolt, Fireball
    "Missiles/Fireba16.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Firebolt, Fireball
    "Missiles/Firewal1.CEL": CelVisualizer(
        width=128, has_skip_header=True
    ),  # Firewall, Flamewave
    "Missiles/Firewal2.CEL": CelVisualizer(
        width=128, has_skip_header=True
    ),  # Firewall, Flamewave
    "Missiles/Lghning.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Lightning, Chain Lightning, Nova
    "Missiles/Manashld.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Mana Shield
    "Missiles/Metlhit1.CEL": CelVisualizer(
        width=96, has_skip_header=True, gif_delay=gif.DIABLO_DELAY * 2
    ),  # TODO this is associated with a missile but is the missile used?
    "Missiles/Metlhit2.CEL": CelVisualizer(
        width=96, has_skip_header=True, gif_delay=gif.DIABLO_DELAY * 2
    ),  # TODO this is associated with a missile but is the missile used?
    "Missiles/Metlhit3.CEL": CelVisualizer(
        width=96, has_skip_header=True, gif_delay=gif.DIABLO_DELAY * 2
    ),  # TODO this is associated with a missile but is the missile used?
    "Missiles/Mindmace.CEL": CelSpriteSheetVisualizer(
        width=96, has_skip_header=True
    ),  # unused, I chose CelSpriteSheetVisualizer since GIF animates too fast at 20 FPS and it has 8 frames (number of cardinal directions)
    "Missiles/Newexp.CEL": CelVisualizer(width=96, has_skip_header=True),  # Apocalypse
    "Missiles/Portal.CEL": CelVisualizer(width=96, has_skip_header=True),  # Town Portal
    "Missiles/Portalu.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Town Portal
    "Missiles/Sentfr.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Sentinel, Guardian
    "Missiles/Sentout.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Sentinel, Guardian
    "Missiles/Sentup.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # Sentinel, Guardian
    "Missiles/Shatter1.CEL": CelVisualizer(
        width=128, has_skip_header=True
    ),  # Stone curse
    # TODO unique monsters
    # TODO monster variant TRNs
    "Monsters/Bat/BatA.CEL": GroupedCelVisualizer(
        width=96, groups=8, has_skip_header=True
    ),
    "Monsters/Bat/BatD.CEL": GroupedCelVisualizer(
        width=96, groups=8, has_skip_header=True
    ),
    "Monsters/Bat/BatH.CEL": GroupedCelVisualizer(
        width=96, groups=8, has_skip_header=True
    ),
    "Monsters/Bat/BatN.CEL": GroupedCelVisualizer(
        width=96, groups=8, has_skip_header=True
    ),
    "Monsters/Bat/BatW.CEL": GroupedCelVisualizer(
        width=96, groups=8, has_skip_header=True
    ),
    "Monsters/FalSpear/PhallA.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/FalSpear/PhallD.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/FalSpear/PhallH.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/FalSpear/PhallN.CEL": GroupedCelVisualizer(
        width=128,
        groups=8,
        has_skip_header=True,
        gif_delay=gif.DIABLO_DELAY * 3,
    ),
    "Monsters/FalSpear/PhallS.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/FalSpear/PhallW.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/FalSword/FallA.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/FalSword/FallD.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/FalSword/FallH.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/FalSword/FallN.CEL": GroupedCelVisualizer(
        width=128,
        groups=8,
        has_skip_header=True,
        gif_delay=gif.DIABLO_DELAY * 3,
    ),
    "Monsters/FalSword/FallS.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/FalSword/FallW.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/FatC/FatCA.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/FatC/FatCD.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/FatC/FatCH.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/FatC/FatCN.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/FatC/FatCW.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/Magma/MagBlos.CEL": CelVisualizer(width=128, has_skip_header=True),
    "Monsters/Magma/Magball1.CEL": CelVisualizer(width=128, has_skip_header=True),
    "Monsters/Magma/Magball2.CEL": CelVisualizer(width=128, has_skip_header=True),
    "Monsters/Magma/Magball3.CEL": CelVisualizer(width=128, has_skip_header=True),
    "Monsters/Magma/Magball4.CEL": CelVisualizer(width=128, has_skip_header=True),
    "Monsters/Magma/Magball5.CEL": CelVisualizer(width=128, has_skip_header=True),
    "Monsters/Magma/Magball6.CEL": CelVisualizer(width=128, has_skip_header=True),
    "Monsters/Magma/Magball7.CEL": CelVisualizer(width=128, has_skip_header=True),
    "Monsters/Magma/Magball8.CEL": CelVisualizer(width=128, has_skip_header=True),
    "Monsters/Scav/ScavA.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/Scav/ScavD.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/Scav/ScavH.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/Scav/ScavN.CEL": GroupedCelVisualizer(
        width=128,
        groups=8,
        has_skip_header=True,
        gif_delay=gif.DIABLO_DELAY * 2,
    ),
    "Monsters/Scav/ScavS.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/Scav/ScavW.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/SkelAxe/SklAxA.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/SkelAxe/SklAxD.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/SkelAxe/SklAxH.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/SkelAxe/SklAxN.CEL": GroupedCelVisualizer(
        width=128,
        groups=8,
        has_skip_header=True,
        gif_delay=gif.DIABLO_DELAY * 4,
    ),  # TODO gif_delay is different for each skeleton variant
    "Monsters/SkelAxe/SklAxS.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/SkelAxe/SklAxW.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/SkelBow/SklBwA.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/SkelBow/SklBwD.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/SkelBow/SklBwH.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/SkelBow/SklBwN.CEL": GroupedCelVisualizer(
        width=128,
        groups=8,
        has_skip_header=True,
        gif_delay=gif.DIABLO_DELAY * 4,
    ),  # TODO gif_delay is different for each skeleton variant
    "Monsters/SkelBow/SklBwS.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/SkelBow/SklBwW.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/SkelSd/SklSrA.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/SkelSd/SklSrD.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/SkelSd/SklSrH.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/SkelSd/SklSrN.CEL": GroupedCelVisualizer(
        width=128,
        groups=8,
        has_skip_header=True,
        gif_delay=gif.DIABLO_DELAY * 4,
    ),
    "Monsters/SkelSd/SklSrS.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/SkelSd/SklSrW.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/Zombie/ZombieA.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/Zombie/ZombieD.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/Zombie/ZombieH.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Monsters/Zombie/ZombieN.CEL": GroupedCelVisualizer(
        width=128,
        groups=8,
        has_skip_header=True,
        gif_delay=gif.DIABLO_DELAY * 4,
    ),
    "Monsters/Zombie/ZombieS.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),  # unused; similar to ZombieD.CEL but the SHA is different
    "Monsters/Zombie/ZombieW.CEL": GroupedCelVisualizer(
        width=128, groups=8, has_skip_header=True
    ),
    "Objects/Angel.CEL": CelSpriteSheetHorizontalVisualizer(
        width=96, has_skip_header=True
    ),  # unused (present in AllObjects) TODO width is wrong, 2 frames should be merged
    "Objects/Banner.CEL": CelSpriteSheetVisualizer(width=96, has_skip_header=True),
    "Objects/Barrel.CEL": CelVisualizer(width=96, has_skip_header=True),
    "Objects/Barrelex.CEL": CelVisualizer(width=96, has_skip_header=True),
    "Objects/BCase.CEL": CelSpriteSheetVisualizer(width=96, has_skip_header=True),
    "Objects/bkurns.CEL": CelSpriteSheetVisualizer(
        width=96, has_skip_header=True
    ),  # unused
    "Objects/BloodFnt.CEL": CelVisualizer(
        width=96, has_skip_header=True, gif_delay=gif.DIABLO_DELAY * 2
    ),
    "Objects/Book1.CEL": CelSpriteSheetVisualizer(width=96, has_skip_header=True),
    "Objects/Book2.CEL": CelSpriteSheetVisualizer(width=96, has_skip_header=True),
    "Objects/BShelf.CEL": CelSpriteSheetVisualizer(
        width=96, has_skip_header=True
    ),  # unused (present in ObjMasterLoadList)
    "Objects/Burncros.CEL": CelVisualizer(width=160, has_skip_header=True),
    "Objects/candlabr.CEL": CelVisualizer(width=96, has_skip_header=True),  # unused
    "Objects/candle.CEL": CelSpriteSheetVisualizer(
        width=96, has_skip_header=True
    ),  # unused; TODO contains 3 animations (new, half-used, spent)
    "Objects/Candle2.CEL": CelVisualizer(
        width=96, has_skip_header=True, gif_delay=gif.DIABLO_DELAY * 2
    ),  # TODO check if this is part of candle.cel
    "Objects/cauldren.CEL": CelSpriteSheetVisualizer(
        width=96, has_skip_header=True
    ),  # unused
    "Objects/Chest1.CEL": CelSpriteSheetVisualizer(width=96, has_skip_header=True),
    "Objects/Chest2.CEL": CelSpriteSheetVisualizer(width=96, has_skip_header=True),
    "Objects/Chest3.CEL": CelSpriteSheetVisualizer(width=96, has_skip_header=True),
    "Objects/CruxSk1.CEL": CelVisualizer(width=96, has_skip_header=True),
    "Objects/CruxSk2.CEL": CelVisualizer(width=96, has_skip_header=True),
    "Objects/CruxSk3.CEL": CelVisualizer(width=96, has_skip_header=True),
    "Objects/Decap.CEL": CelSpriteSheetVisualizer(width=96, has_skip_header=True),
    "Objects/dirtfall.CEL": CelVisualizer(
        width=96, has_skip_header=True, gif_delay=gif.DIABLO_DELAY * 2
    ),  # unused; delay is a guess
    "Objects/explod1.CEL": CelVisualizer(width=128, has_skip_header=True),  # unused
    "Objects/explod2.CEL": CelVisualizer(width=128, has_skip_header=True),  # unused
    "Objects/firewal1.CEL": CelVisualizer(
        width=160, has_skip_header=True
    ),  # unused; very similar to Missiles/firewal1.CEL
    "Objects/Flame1.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # unused (present in AllObjects) TODO are there multiple animations in this?
    "Objects/flame3.CEL": CelVisualizer(width=96, has_skip_header=True),  # unused
    "Objects/ghost.CEL": CelVisualizer(width=128, has_skip_header=True),  # unused
    "Objects/L1Braz.CEL": CelVisualizer(width=64, has_skip_header=True),
    "Objects/L1Doors.CEL": CelSpriteSheetVisualizer(width=64, has_skip_header=True),
    # 'Objects/L2Doors.CEL': CelSpriteSheetVisualizer(width=64, palette_key='Levels/L2Data/L2_1.pal', has_skip_header=True), # TODO This requries a palette that's not in the demo DIABDAT.MPQ!
    "Objects/Lever.CEL": CelSpriteSheetVisualizer(
        width=96, has_skip_header=True
    ),  # TODO is this used? it's in AllObjects with OLOAD_YES...
    "Objects/LShrineG.CEL": CelSpriteSheetVisualizer(
        width=128, has_skip_header=True
    ),  # TODO this file contains 2 animations
    "Objects/MiniWatr.CEL": CelVisualizer(
        width=64, has_skip_header=True
    ),  # unused (present in ObjMasterLoadList)
    "Objects/Nude2.CEL": CelVisualizer(
        width=128, has_skip_header=True, gif_delay=gif.DIABLO_DELAY * 3
    ),  # TODO is this used? it's in AllObjects with OLOAD_NO
    "Objects/Prsrplt1.CEL": CelSpriteSheetVisualizer(
        width=96, has_skip_header=True
    ),  # unused (present in ObjMasterLoadList)
    "Objects/Rockstan.CEL": CelVisualizer(width=96, has_skip_header=True),
    "Objects/RShrineG.CEL": CelSpriteSheetVisualizer(
        width=128, has_skip_header=True
    ),  # TODO this file contains 2 animations
    "Objects/Sarc.CEL": CelVisualizer(
        width=128, has_skip_header=True, gif_delay=gif.DIABLO_DELAY * 3
    ),
    "Objects/SkulFire.CEL": CelVisualizer(
        width=96, has_skip_header=True, gif_delay=gif.DIABLO_DELAY * 2
    ),
    "Objects/SkulPile.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # TODO is this used? it's in AllObjects with OLOAD_NO
    "Objects/SkulStik.CEL": CelSpriteSheetVisualizer(
        width=96, has_skip_header=True
    ),  # unused (present in ObjMasterLoadList)
    "Objects/switch2.CEL": CelSpriteSheetVisualizer(
        width=96, has_skip_header=True
    ),  # unused
    "Objects/switch3.CEL": CelSpriteSheetVisualizer(
        width=96, has_skip_header=True
    ),  # unused
    "Objects/Switch4.CEL": CelSpriteSheetVisualizer(
        width=96, has_skip_header=True
    ),  # used in the Bone Chamber
    "Objects/TNudeM.CEL": CelSpriteSheetVisualizer(
        width=128, has_skip_header=True
    ),  # TODO: Is THEME_TORTURE ever generated?
    "Objects/TNudeW.CEL": CelSpriteSheetVisualizer(
        width=128, has_skip_header=True
    ),  # TODO: Is THEME_TORTURE ever generated?
    "Objects/Traphole.CEL": CelSpriteSheetVisualizer(width=64, has_skip_header=True),
    "Objects/TSoul.CEL": CelSpriteSheetVisualizer(width=128, has_skip_header=True),
    "Objects/vapor1.CEL": CelSpriteSheetHorizontalVisualizer(
        width=128, has_skip_header=True
    ),  # unused TODO this file is broken
    "Objects/water.CEL": CelVisualizer(width=128, has_skip_header=True),  # unused
    "Objects/waterjug.CEL": CelSpriteSheetVisualizer(
        width=96, has_skip_header=True
    ),  # unused
    "Objects/WeapStnd.CEL": CelSpriteSheetHorizontalVisualizer(
        width=96, has_skip_header=True
    ),  # unused (present in AllObjects) TODO broken
    # 'Objects/WTorch1.CEL': CelVisualizer(width=96, has_skip_header=True), # TODO This requries a palette that's not in the demo DIABDAT.MPQ!
    # 'Objects/WTorch2.CEL': CelVisualizer(width=96, has_skip_header=True), # TODO This requries a palette that's not in the demo DIABDAT.MPQ!
    # 'Objects/WTorch3.CEL': CelVisualizer(width=96, has_skip_header=True), # TODO This requries a palette that's not in the demo DIABDAT.MPQ!
    # 'Objects/WTorch4.CEL': CelVisualizer(width=96, has_skip_header=True), # TODO This requries a palette that's not in the demo DIABDAT.MPQ!
    # TODO PlrGFX
    "PlrGFX/Warrior/WLA/WLADT.CEL": GroupedCelVisualizer(
        width=128,
        groups=8,
        has_skip_header=True,
        gif_delay=gif.DIABLO_DELAY * 2,
    ),
    "PlrGFX/Warrior/WLB/WLBDT.CEL": GroupedCelVisualizer(
        width=128,
        groups=8,
        has_skip_header=True,
        gif_delay=gif.DIABLO_DELAY * 2,
    ),
    "PlrGFX/Warrior/WLD/WLDDT.CEL": GroupedCelVisualizer(
        width=128,
        groups=8,
        has_skip_header=True,
        gif_delay=gif.DIABLO_DELAY * 2,
    ),
    "PlrGFX/Warrior/WLH/WLHDT.CEL": GroupedCelVisualizer(
        width=128,
        groups=8,
        has_skip_header=True,
        gif_delay=gif.DIABLO_DELAY * 2,
    ),
    "PlrGFX/Warrior/WLM/WLMDT.CEL": GroupedCelVisualizer(
        width=128,
        groups=8,
        has_skip_header=True,
        gif_delay=gif.DIABLO_DELAY * 2,
    ),
    "PlrGFX/Warrior/WLN/WLNDT.CEL": GroupedCelVisualizer(
        width=128,
        groups=8,
        has_skip_header=True,
        gif_delay=gif.DIABLO_DELAY * 2,
    ),
    "PlrGFX/Warrior/WLS/WLSDT.CEL": GroupedCelVisualizer(
        width=128,
        groups=8,
        has_skip_header=True,
        gif_delay=gif.DIABLO_DELAY * 2,
    ),
    "PlrGFX/Warrior/WLT/WLTDT.CEL": GroupedCelVisualizer(
        width=128,
        groups=8,
        has_skip_header=True,
        gif_delay=gif.DIABLO_DELAY * 2,
    ),
    "PlrGFX/Warrior/WLU/WLUDT.CEL": WludtVisualizer(
        width=128,
        groups=8,
        has_skip_header=True,
        gif_delay=gif.DIABLO_DELAY * 2,
    ),
    "Spells/Lghning.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # unused; same as Missiles/lghning.cel
    "Spells/Manashld.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # unused; same as Missiles/manashld.cel
    "Spells/Mindmace.CEL": CelSpriteSheetVisualizer(
        width=96, has_skip_header=True
    ),  # unused; same as Missiles/mindmance.cel
    "Spells/Newexp.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # unused; same as Missiles/newexp.cel
    "Spells/Portal.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # unused; same as Missiles/portal.cel
    "Spells/Portalu.CEL": CelVisualizer(
        width=96, has_skip_header=True
    ),  # unused; same as Missiles/portalu.cel
    # TODO: towners
    # 'towners/animals/cow.cel': GroupedCelVisualizer(width=128, groups=8, has_skip_header=True, gif_delay=gif.DIABLO_DELAY * 2), # TODO: delay
}

#
# Main
#


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("mpq_file")
    parser.add_argument("--file")
    parser.add_argument("--out", type=Path, default=Path("./visualize"))
    args = parser.parse_args()

    if args.file:
        file_regex = re.compile(args.file)

    stormlib_path = (
        Path(__file__).resolve().parent.parent
        / "prebuilts"
        / "StormLib-Win64"
        / "StormLib.dll"
    )
    stormlib = WinDLL(str(stormlib_path))
    with mpq.open_archive(stormlib, "DIABDAT.MPQ") as archive:
        for key in _METADATA:
            # Support --file (kinda hacky tbh)
            # TODO support regex/glob

            if args.file and not file_regex.match(key):
                continue

            out_path = args.out / key
            out_path.parent.mkdir(parents=True, exist_ok=True)

            mpq_key = key.replace("/", "\\")
            print(mpq_key)

            visualizer = _METADATA[key]
            visualizer.prepare(archive)
            visualizer.visualize(archive.read_file(mpq_key), out_path)


if __name__ == "__main__":
    main()
