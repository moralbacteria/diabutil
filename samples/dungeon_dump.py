from argparse import ArgumentParser
from ctypes import c_void_p, windll, WinDLL
from ctypes.wintypes import BYTE
from pathlib import Path

from dun_render import dun_render

import diabutil
from diabutil import cel, dun, mpq, pal, pnm, til


_PROCESS_VM_READ = 0x10

_DMAXX = 40
_DMAXY = 40

_DUNGEON_ADDRESS = 0x004E6760


def _read_process_memory(pid: int, address: int, size: int) -> bytes:
    handle = windll.kernel32.OpenProcess(_PROCESS_VM_READ, False, pid)
    if not handle:
        raise RuntimeError("OpenProcess failed")

    data = (BYTE * size)()
    if not windll.kernel32.ReadProcessMemory(
        handle, c_void_p(address), data, size, None
    ):
        raise RuntimeError(
            f"ReadProcessMemory failed: {windll.kernel32.GetLastError()}"
        )

    return bytes(data)


def _dungeon_dump(pid: int) -> bytes:
    return _read_process_memory(pid, _DUNGEON_ADDRESS, _DMAXX * _DMAXY)


def _transpose(dun_file: dun.DunFile) -> dun.DunFile:
    transposed = dun.DunFile(dun_file.width, dun_file.height, dun_file.tiles[:])
    for y in range(dun_file.height):
        for x in range(dun_file.width):
            dun_file_data = dun_file.tiles[y * dun_file.width + x]
            transposed.tiles[x * dun_file.height + y] = dun_file_data
    return transposed


def main():
    parser = ArgumentParser()
    # You can find PID using Task Manager
    # TODO: Search process list for DIABLO.EXE
    parser.add_argument("pid", type=int)
    args = parser.parse_args()

    # Dump dungeon memory from a running process
    dun_file = _transpose(dun.DunFile(_DMAXX, _DMAXY, list(_dungeon_dump(args.pid))))

    # Load files to render the dungeon
    stormlib_path = (
        Path(__file__).resolve().parent.parent
        / "prebuilts"
        / "StormLib-Win64"
        / "StormLib.dll"
    )
    stormlib = WinDLL(str(stormlib_path))
    with mpq.open_archive(stormlib, "DIABDAT.MPQ") as archive:
        # TODO switch to load L2/L3 (from beta? pre-ablo?)
        all_micros = diabutil.min.decode_min(
            archive.read_file("Levels\\L1Data\\L1.MIN")
        )
        all_tiles = til.decode_til(archive.read_file("Levels\\L1Data\\L1.TIL"))
        palette = pal.parse_palette(archive.read_file("Levels\\L1Data\\L1_1.PAL"))
        ldata_cel = cel.find_frame_data(archive.read_file("Levels\\L1Data\\L1.CEL"))
        ldata_s_cel = cel.decode_frame_data(
            archive.read_file("Levels\\L1Data\\L1S.CEL"), has_skip_header=True
        )

    # Render and save
    animation = dun_render(
        all_micros, all_tiles, palette, ldata_cel, ldata_s_cel, dun_file
    )
    ppm_data = bytes(
        [x for pixel in animation.frames[0] for x in animation.palette[pixel].encode()]
    )
    pnm.save_ppm_p6(animation.width, animation.height, ppm_data, Path("dungeon.ppm"))


if __name__ == "__main__":
    main()
