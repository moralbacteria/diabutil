"""These indices are 0-based (as opposed to many others like .TIL which are 1-based )

Entries are drawn in this location in this order:

 1
3 2
 4
"""

WORD_SIZE = 2

Tile = list[int]
TILE_SIZE = 4
TILE_WIDTH_PX = 128
TILE_HALF_WIDTH_PX = 64
TILE_HEIGHT_PX = 192


def _as_word(data: bytes):
    return int.from_bytes(data, "little")


def decode_til(data: bytes) -> list[Tile]:
    builder: list[Tile] = []
    for i in range(0, len(data), TILE_SIZE * WORD_SIZE):
        builder.append(
            [
                _as_word(data[i + WORD_SIZE * 0 : i + WORD_SIZE * 1]),
                _as_word(data[i + WORD_SIZE * 1 : i + WORD_SIZE * 2]),
                _as_word(data[i + WORD_SIZE * 2 : i + WORD_SIZE * 3]),
                _as_word(data[i + WORD_SIZE * 3 : i + WORD_SIZE * 4]),
            ]
        )
    return builder
