from typing import Any, Generator

_INT8_SIZE = 1
_UINT8_SIZE = 1
_UINT16_SIZE = 2
_UINT32_SIZE = 4


class BufferedIterator:
    """A wrapper around bytes that provides CEL and CL2-specific helper
    functions and meaningful error messages"""

    def __init__(self, data: bytes):
        self._data = data
        self._index = 0
        self._length = len(data)

    def next_int8(self) -> int:
        """Read 1 byte as signed"""
        if self._index + _INT8_SIZE > self._length:
            raise RuntimeError("Tried to read int8 but reached end of data!")
        retval = int.from_bytes(
            self._data[self._index : self._index + _INT8_SIZE],
            "little",
            signed=True,
        )
        self._index += _INT8_SIZE
        return retval

    def next_uint8(self) -> int:
        """Read 1 byte as unsigned"""
        if self._index + _UINT8_SIZE > self._length:
            raise RuntimeError("Tried to read int8 but reached end of data!")
        retval = self._data[self._index]
        self._index += _UINT8_SIZE
        return retval

    def next_uint16(self) -> int:
        """Read 2 bytes as unsigned little endian"""
        if self._index + _UINT16_SIZE > self._length:
            raise RuntimeError("Tried to read uint16 but reached end of data!")
        retval = int.from_bytes(
            self._data[self._index : self._index + _UINT16_SIZE], "little"
        )
        self._index += _UINT16_SIZE
        return retval

    def next_uint32(self) -> int:
        """Read 4 bytes as unsigned little endian"""
        if self._index + _UINT32_SIZE > self._length:
            raise RuntimeError("Tried to read uint32 but reached end of data!")
        retval = int.from_bytes(
            self._data[self._index : self._index + _UINT32_SIZE], "little"
        )
        self._index += _UINT32_SIZE
        return retval

    def next_bytes(self, length: int) -> bytes:
        """Return a sequence of `length` bytes"""
        if self._index + length > self._length:
            raise RuntimeError("Tried to read bytes but reached end of data!")
        retval = self._data[self._index : self._index + length]
        self._index += length
        return retval

    def seek(self, index: int):
        if index > self._length:
            raise RuntimeError("seeking past end of data!")
        self._index = index

    def tell(self) -> int:
        return self._index

    def at_eof(self) -> bool:
        return self._index >= len(self._data)


def pair_sliding_window(
    data: list[Any],
) -> Generator[tuple[Any, Any], None, None]:
    """Use a sliding window of two values to iterate over a list. For a
    list of size n, can call next() n-1 times. Each next() returns
    (data[i], data[i+1])

    Example:
    >>> window = pair_sliding_window([1, 2, 3, 4])
    >>> next(window)
    (1, 2)
    >>> next(window)
    (2, 3)
    >>> next(window)
    (3, 4)
    """
    for i in range(len(data) - 1):
        yield (data[i], data[i + 1])


def find_groups(iterator: BufferedIterator, num_groups: int) -> list[int]:
    iterator.seek(0)
    return [iterator.next_uint32() for _ in range(num_groups)]


def find_frames(iterator: BufferedIterator) -> list[int]:
    num_frames = iterator.next_uint32()
    frames = [iterator.next_uint32() for _ in range(num_frames + 1)]
    return frames
