"""TODO"""
from .cel import rle_decode_sequence
from .util import BufferedIterator
from .types import PseudoColorSequence, TRANSPARENT_PIXEL, CelRleSequence

from dataclasses import dataclass
from enum import Enum

MIN_ENTRY_SIZE = 2
# TODO: Don't assume 10 micros (only used in dungeon)
MIN_ENTRY_PER_MICROS = 10
MICROS_SIZE = MIN_ENTRY_SIZE * MIN_ENTRY_PER_MICROS

MIN_WIDTH_PX = 32
MIN_FINAL_HEIGHT_PX = 32

MICROS_RENDERED_WIDTH = 64
MICROS_RENDERED_HEIGHT = 160


class MicroType(Enum):
    """Data is RAW, not RLE encoded"""

    SOLID = 0
    RLE_DATA = 1
    LEFT_TRIANGLE = 2
    RIGHT_TRANGLE = 3
    LEFT_TRIANGLE_TO_WALL = 4
    RIGHT_TRIANGLE_TO_WALL = 5


@dataclass
class Micro:
    """Index into Levels/L*Data/L*.CEL"""

    number: int
    """How to render"""
    type: MicroType

    @classmethod
    def decode(cls, data: int):
        return cls(number=(data & 0x0FFF), type=MicroType((data & 0x7000) >> 12))


# The length is either 10 (dungeon) or 16 (town)
Micros = list[Micro]


def _decode_solid(data: bytes) -> PseudoColorSequence:
    # Nothing to do :)
    return list(data)


def _decode_rle(data: bytes) -> PseudoColorSequence:
    return rle_decode_sequence(data)


def _decode_left_triangle(data: bytes) -> PseudoColorSequence:
    builder: PseudoColorSequence = []
    iterator = BufferedIterator(data)

    for y in range(MIN_FINAL_HEIGHT_PX):
        transparency_count = abs(MIN_WIDTH_PX - ((y + 1) * 2))
        copy_count = MIN_WIDTH_PX - transparency_count

        builder += [TRANSPARENT_PIXEL] * transparency_count
        builder += list(iterator.next_bytes(copy_count))

    return builder


def _decode_right_triangle(data: bytes) -> PseudoColorSequence:
    builder: PseudoColorSequence = []
    iterator = BufferedIterator(data)

    for y in range(MIN_FINAL_HEIGHT_PX):
        transparency_count = abs(MIN_WIDTH_PX - ((y + 1) * 2))
        copy_count = MIN_WIDTH_PX - transparency_count

        builder += list(iterator.next_bytes(copy_count))
        builder += [TRANSPARENT_PIXEL] * transparency_count

    return builder


def _decode_left_triangle_to_wall(data: bytes) -> PseudoColorSequence:
    builder: PseudoColorSequence = []
    iterator = BufferedIterator(data)

    for y in range(MIN_FINAL_HEIGHT_PX):
        transparency_count = max(0, MIN_WIDTH_PX - (y + 1) * 2)
        copy_count = MIN_WIDTH_PX - transparency_count

        builder += [TRANSPARENT_PIXEL] * transparency_count
        builder += list(iterator.next_bytes(copy_count))

    return builder


def _decode_right_triangle_to_wall(data: bytes) -> PseudoColorSequence:
    builder: PseudoColorSequence = []
    iterator = BufferedIterator(data)

    for y in range(MIN_FINAL_HEIGHT_PX):
        transparency_count = max(0, MIN_WIDTH_PX - (y + 1) * 2)
        copy_count = MIN_WIDTH_PX - transparency_count

        builder += list(iterator.next_bytes(copy_count))
        builder += [TRANSPARENT_PIXEL] * transparency_count

    return builder


def decode_mt_cel(data: bytes, type: MicroType) -> PseudoColorSequence:
    match type:
        case MicroType.SOLID:
            return _decode_solid(data)
        case MicroType.RLE_DATA:
            return _decode_rle(data)
        case MicroType.LEFT_TRIANGLE:
            return _decode_left_triangle(data)
        case MicroType.RIGHT_TRANGLE:
            return _decode_right_triangle(data)
        case MicroType.LEFT_TRIANGLE_TO_WALL:
            return _decode_left_triangle_to_wall(data)
        case MicroType.RIGHT_TRIANGLE_TO_WALL:
            return _decode_right_triangle_to_wall(data)
    raise RuntimeError("not implemented")


def decode_micros(data: bytes) -> Micros:
    return [
        Micro.decode(int.from_bytes(data[i : i + MIN_ENTRY_SIZE], "little"))
        for i in range(0, len(data), MIN_ENTRY_SIZE)
    ]


def decode_min(data: bytes) -> list[Micros]:
    return [
        decode_micros(data[micros_index : micros_index + MICROS_SIZE])
        for micros_index in range(0, len(data), MICROS_SIZE)
    ]


def _render_mt(mt: Micro, dun_cel: list[CelRleSequence]) -> PseudoColorSequence:
    return decode_mt_cel(dun_cel[mt.number - 1], mt.type)


def render_micros(micros: Micros, dun_cel: list[CelRleSequence]) -> PseudoColorSequence:
    blank_template = [TRANSPARENT_PIXEL] * (MIN_WIDTH_PX * MIN_FINAL_HEIGHT_PX)

    builder: PseudoColorSequence = []
    for mt_row in range(int(len(micros) / 2)):
        # Render from the bottom up
        micros_index = len(micros) - (mt_row + 1) * 2
        left_mt = micros[micros_index]
        right_mt = micros[micros_index + 1]

        left_frame = (
            blank_template if left_mt.number == 0 else _render_mt(left_mt, dun_cel)
        )
        right_frame = (
            blank_template if right_mt.number == 0 else _render_mt(right_mt, dun_cel)
        )

        for y in range(MIN_FINAL_HEIGHT_PX):
            frame_index = y * MIN_WIDTH_PX
            builder += left_frame[frame_index : frame_index + MIN_WIDTH_PX]
            builder += right_frame[frame_index : frame_index + MIN_WIDTH_PX]

    return builder
