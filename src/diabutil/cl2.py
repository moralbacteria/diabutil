"""Functions for decoding CL2 graphics files.

There's many differences compared to CEL:

0. The frame headers are relative to the group offset from the group header. This means can't reuse the frame finding function
1. Data is organized differently: group header, then all frame headers, then all frame data
2. There is one more op-code: fill. This repeats the next byte
3. Decoding run lengths: transparency > 0, values < 0 split between copy and fill
"""

from . import PseudoColorAnimation
from .pal import Palette
from .types import TRANSPARENT_PIXEL, PseudoColorSequence
from .util import (
    BufferedIterator,
    pair_sliding_window,
    find_groups,
    find_frames,
)

from dataclasses import dataclass
from enum import Enum


class _OpCode(Enum):
    TRANSPARENCY = 1
    COPY = 2
    FILL = 3


@dataclass
class _Instruction:
    opcode: _OpCode
    length: int

    @classmethod
    def decode(cls, data: int):
        if data == 0:
            raise RuntimeError("Invalid CL2 instruction: 0")

        if data > 0:
            return cls(_OpCode.TRANSPARENCY, data)

        data = -data
        if data > 65:
            return cls(_OpCode.FILL, data - 65)

        return cls(_OpCode.COPY, data)


class _RleDecoder:
    """Uses the bytes iterator to produce runs"""

    def __init__(self, iterator: BufferedIterator):
        self._iterator = iterator

    def next_run(self) -> PseudoColorSequence:
        instruction = _Instruction.decode(self._iterator.next_int8())
        match instruction.opcode:
            case _OpCode.TRANSPARENCY:
                return [TRANSPARENT_PIXEL] * instruction.length
            case _OpCode.COPY:
                return [x for x in self._iterator.next_bytes(instruction.length)]
            case _OpCode.FILL:
                return [self._iterator.next_uint8()] * instruction.length
        raise RuntimeError(f"Invalid opcode: {instruction.opcode}")


def _rle_decode(iterator: BufferedIterator, end: int) -> PseudoColorSequence:
    """Decode .CL2 RLE data into an uncompressed list of palette indices.

    Arguments:
        data: Raw RLE data from the .CL2 file (without skip header)

    Returns:
        Uncompressed image data. The is laid out bottom-to-top so most 2D
        frameworks will want the image vertically flipped. This data does not
        include image dimensions. Transparent pixels are -1
    """
    builder: PseudoColorSequence = []
    decoder = _RleDecoder(iterator)
    while iterator.tell() < end:
        run = decoder.next_run()
        builder += run
    return builder


# TODO: are there any non-grouped CL2 files?
def deserialize_with_groups(
    data: bytes,
    palette: Palette,
    width: int,
    num_groups: int,
    has_skip_header: bool = False,
) -> list[PseudoColorAnimation]:
    builder = []

    iterator = BufferedIterator(data)

    group_offsets = find_groups(iterator, num_groups)
    for group_offset in group_offsets:
        decoded_frames = []
        iterator.seek(group_offset)
        # frame offsets are relative; make them absolute
        frame_offsets = [x + group_offset for x in find_frames(iterator)]
        for frame_begin_offset, frame_end_offset in pair_sliding_window(frame_offsets):
            if has_skip_header:
                frame_begin_offset += 10
            iterator.seek(frame_begin_offset)
            decoded_frames.append(_rle_decode(iterator, frame_end_offset))

        height = int(len(decoded_frames[0]) / width)
        animation = PseudoColorAnimation(width, height, decoded_frames, palette)
        animation.flip_vertically()

        builder.append(animation)

    return builder
