"""Simple structs and type aliases commonly used throughout diabutil"""

from dataclasses import dataclass


# Sentinel used for transparent pixel in PseudoColorSequence
TRANSPARENT_PIXEL = -1
# Size in bytes of skip header on a CEL Frame
SKIP_HEADER_SIZE = 10


@dataclass
class Color:
    """RGB representation of digital color"""

    red: int
    green: int
    blue: int

    def encode(self) -> bytes:
        """3 bytes: bytes([self.red, self.green, self.blue])"""
        return bytes([self.red, self.green, self.blue])

    @classmethod
    def from_bytes(cls, data: bytes):
        """Color(data[0], data[1], data[2])"""
        return cls(data[0], data[1], data[2])


# How many bytes are in an encoded Color
COLOR_ENCODED_SIZE = 3

# This is pseudo-color data that is RLE compressed.
CelRleSequence = bytes
Cl2RleSequence = bytes

# A palette is an indexed list of colors. Typically it is 256 long (for 8-bit
# graphics) however it might be smaller than that if the image uses fewer colors
Palette = list[Color]

# Each pseudo-color pixel is a palette index. It has a value from [0..255], and
# -1 for transparency
PseudoColorPixel = int
# A sequence of pixels with one byte per pixel. Each byte references some
# external color palette. -1 is transprent. Typically stored in scan lines.
# Does not include image dimensions
PseudoColorSequence = list[PseudoColorPixel]
