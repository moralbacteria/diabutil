"""A palette (.PAL) is a list of 256 RGB colors. Each color component is 8 bits. As a result, the file is always 768 bytes large.

You use the palette to turn the 8-bit graphics into RGB color by using the 8-bit pixel as an index into the palette. For more information regarding a palette's role in 256-color or "pseudocolor" graphics, see https://en.wikipedia.org/wiki/Indexed_color

Diablo palettes have a structure. This often means you can use any ldata palette to load in-game graphics. Brevik's talk goes into more details here: https://youtu.be/VscdPA6sUkc?t=887

> We had these 256 colors to work with, and so we kinda split the palette into two different sections. We split it into 128 colors for the background and 128 colors for everything else (that was the monsters, the UI, the characters, the spell effects) ...

We typically allow palettes to have any number of colors to support, e.g. uncompressed GIFs with only 7-bit color tables.
"""

from .types import Color, Palette, COLOR_ENCODED_SIZE


def encode_palette(palette: Palette) -> bytes:
    """Convert a list of colors into binary, one byte per color component (3
    bytes per color):

        RGBRGBRGBRGB...

    len(encode_palette(foo)) == len(foo) * 3
    """
    builder = b""
    for color in palette:
        builder += color.encode()
    return builder


def decode_pal(data: bytes) -> Palette:
    """Read 256 RGB tuples from a stream of bytes.

    Example:

        from pathlib import Path
        from diabtutil import pal

        pal_path = Path('Levels') / 'L1Data' / 'L1_1.PAL'
        palette = pal.parse_palette(pal_path.read_bytes())
    """
    return [
        Color.from_bytes(data[i : i + COLOR_ENCODED_SIZE])
        for i in range(0, len(data), COLOR_ENCODED_SIZE)
    ]
