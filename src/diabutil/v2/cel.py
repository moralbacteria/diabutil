"""CEL is a container for 8-bit pseudo-color animation data stored with run-length encoding (RLE).

RLE compresses transparent runs, significantly reducing filesize compared to plain list of 8-bit pixels. It also optimizes the blitting function since no work has to be done for transparent pixels.

There's so many types of graphics stored in CEL that it's basically useless as a format:

- Looping animations like PentSpin.cel that share dimensions
- Logical group of individual images with differing dimensions like objcurs.cel
- A bunch of animations crammed into one file, like all plrgfx. This case is special because there's an additional header which contains offsets to each animation sequence. The data that the offset points to can be treated as one of the above CELs.
- Dungeon tile CELs which have an implicit shape (defined elsewhere)

CELs also lack important metadata:

- Whether the frames have a skip header
- Image dimensions
- Which palette to use
- Whether it is a CEL or a collection of CELs

As such, this API can be quite annoying to use since you are expected to do everything. Take PentSpin as an example:

    # Given: `PentSpin` (bytes) is PentSpin.CEL
    # Given:  `palette` (Palette) is an appropriate palette (e.g. L1_1.PAL)

    # Find each group and frame of the PentSpin animation
    cel_rle_frames = cel.decode_cel(PentSpin)
    # PentSpin has no groups but we still return 1 group. As the library author, this makes impl easier
    cel_rle_frames = cel_rle_frames[0]
    # We need to undo the RLE encoded frames that we just found. This requires knowing the width of the final image.
    images = [cel.render_rle(rle, width=48) for rle in cel_rle_frames]
    # Turn this list of images into an animation. This validates that the dimensions are the same for each frame.
    animation = make_animation(images)
    # The GIF encoder can only do 128 colors so discard the unused colors
    animation, palette = gif.compress_palette(animation, palette)
    # So far transparency has been represented as -1. This needs to be added to the palette and passed to the GIF encoder.
    animation, palette, transparency = gif.resolve_transparency(animation, palette)
    # Serialize to disk as GIF
    gif.save_gif(animation, palette, transparency, gif.DIABLO_DELAY, Path("PentSpin.gif"))
"""

from .types import (
    TRANSPARENT_PIXEL,
    CelRleSequence,
    PseudoColorImage,
    PseudoColorSequence,
)
from .util import (
    BufferedIterator,
    pair_sliding_window,
    find_groups,
    find_frames,
    flip_vertically,
)

from dataclasses import dataclass
from enum import Enum


_SKIP_HEADER_SIZE = 10


class _RleOpCode(Enum):
    TRANSPARENCY = 1
    COPY = 2


@dataclass
class _Instruction:
    opcode: _RleOpCode
    length: int

    @classmethod
    def decode(cls, data: int):
        if data < 0:
            return cls(_RleOpCode.TRANSPARENCY, -data)
        return cls(_RleOpCode.COPY, data)


class _RleDecoder:
    """Uses the bytes iterator to produce runs"""

    def __init__(self, iterator: BufferedIterator):
        self._iterator = iterator

    def next_run(self) -> PseudoColorSequence:
        instruction = _Instruction.decode(self._iterator.next_int8())
        match instruction.opcode:
            case _RleOpCode.TRANSPARENCY:
                return [TRANSPARENT_PIXEL] * instruction.length
            case _RleOpCode.COPY:
                return [x for x in self._iterator.next_bytes(instruction.length)]
        raise RuntimeError(f"Invalid instruction: {instruction.opcode}")


def decode_rle(data: CelRleSequence) -> PseudoColorSequence:
    """Decode .CEL RLE data into an uncompressed list of palette indices.

    Arguments:
        data: Raw RLE data from the .CEL file (without skip header)

    Returns:
        Uncompressed image data. The is laid out bottom-to-top so most 2D
        frameworks will want the image vertically flipped. This data does not
        include image dimensions. Transparent pixels are -1
    """
    builder: PseudoColorSequence = []
    iterator = BufferedIterator(data)
    decoder = _RleDecoder(iterator)
    while iterator.has_data():
        builder += decoder.next_run()
    return builder


def decode_cel(data: bytes, num_groups: int = 0) -> list[list[CelRleSequence]]:
    """Chunk a byte-stream into individual RLE-encoded frames.

    Arguments:
        data: The byte stream containing CEL data
        num_groups: Number of groups to except in data. If 0 then no group header

    Returns:
        A list of groups, which is a list of RLE-encoded frames. If num_groups == 0 this will still have 1 group (this simplifies the implementation)
    """

    builder: list[list[CelRleSequence]] = []

    iterator = BufferedIterator(data)
    group_offsets = [0] if num_groups == 0 else find_groups(iterator, num_groups)
    for group_offset in group_offsets:
        rle_frames: list[CelRleSequence] = []

        iterator.seek(group_offset)
        frame_offsets = [x + group_offset for x in find_frames(iterator)]
        for frame_begin_offset, frame_end_offset in pair_sliding_window(frame_offsets):
            iterator.seek(frame_begin_offset)
            frame_size = frame_end_offset - frame_begin_offset
            rle_frames.append(iterator.next_bytes(frame_size))

        builder.append(rle_frames)

    return builder


def render_rle(
    data: bytes, width: int, ignore_skip_header: bool = False
) -> PseudoColorImage:
    """Create a rectangular grid of pixels from an RLE-encoded frame.

    Arguments:
        data: The RLE-encoded frame to turn into an image
        width: The vertical extent of the frame
        ignore_skip_header: If true, ignore the first 10 bytes (skip header). Not every CEL has a skip header

    Return:
        A generic image, a rectangle of pseudo-color pixels
    """
    # We render the whole CEL so the skip header is useless
    if ignore_skip_header:
        data = data[_SKIP_HEADER_SIZE:]
    return PseudoColorImage(width, flip_vertically(decode_rle(data), width))
