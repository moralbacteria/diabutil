"""Simple structs and type aliases commonly used throughout diabutil"""

from dataclasses import dataclass


# Sentinel used for transparent pixel in PseudoColorSequence
TRANSPARENT_PIXEL = -1
# Size in bytes of skip header on a CEL Frame
SKIP_HEADER_SIZE = 10


@dataclass
class Color:
    """RGB representation of digital color"""

    red: int
    green: int
    blue: int

    def encode(self) -> bytes:
        """3 bytes: bytes([self.red, self.green, self.blue])"""
        return bytes([self.red, self.green, self.blue])

    @classmethod
    def from_bytes(cls, data: bytes):
        """Color(data[0], data[1], data[2])"""
        return cls(data[0], data[1], data[2])


# How many bytes are in an encoded Color
COLOR_ENCODED_SIZE = 3

# This is pseudo-color data that is RLE compressed.
CelRleSequence = bytes
Cl2RleSequence = bytes

# A palette is an indexed list of colors. Typically it is 256 long (for 8-bit
# graphics) however it might be smaller than that if the image uses fewer colors
Palette = list[Color]

# Each pseudo-color pixel is a palette index. It has a value from [0..255], and
# -1 for transparency
PseudoColorPixel = int
"""A sequence of pseudo-color pixels with one byte per pixel. Each byte references some
external color palette. -1 is transprent. Typically stored in scan lines (row-major). Does not include image dimensions"""
PseudoColorSequence = list[PseudoColorPixel]


class PseudoColorImage:
    """The image is probably upside-down because that's how Windows rolls B)"""

    def __init__(self, width: int, data: PseudoColorSequence):
        self._width = width
        # data is a rectangle of pixels; height = area / width
        self._height = int(len(data) / width)
        self._data = data

    def blit(self, other, x: int, y: int):
        """Draw other onto self, respecting transparency"""
        for other_y in range(other.get_height()):
            for other_x in range(other.get_width()):
                self_x = other_x + x
                self_y = other_y + y

                other_color = other.at(other_x, other_y)
                if other_color == TRANSPARENT_PIXEL:
                    continue
                self.put(self_x, self_y, other_color)

    def at(self, x: int, y: int) -> PseudoColorPixel:
        return self._data[y * self._width + x]

    def put(self, x: int, y: int, color: PseudoColorPixel):
        self._data[y * self._width + x] = color

    def get_width(self) -> int:
        return self._width

    def get_height(self) -> int:
        return self._height

    def get_data(self) -> PseudoColorSequence:
        return self._data

    @classmethod
    def blank(cls, width: int, height: int):
        return cls(width, height, [TRANSPARENT_PIXEL] * (width * height))


@dataclass
class PseudoColorAnimation:
    """A sequence of frames that share the same dimensions and palette."""

    width: int
    height: int
    frames: list[PseudoColorSequence]


def make_animation(frames: list[PseudoColorImage]) -> PseudoColorAnimation:
    animation = PseudoColorAnimation(frames[0].get_width(), frames[0].get_height(), [])
    for frame in frames:
        assert frame.get_width() == animation.width
        assert frame.get_height() == animation.height
        animation.frames.append(frame.get_data())
    return animation
