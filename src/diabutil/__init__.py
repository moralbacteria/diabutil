"""Utilities for loading Diablo file formats"""

from .types import (
    Palette,
    PseudoColorSequence,
    PseudoColorPixel,
    TRANSPARENT_PIXEL,
)
from dataclasses import dataclass
from typing import Any


def _flip_vertically(data: list[Any], width: int, height: int) -> list[Any]:
    """Flip an image vertically; the first row becomes the last, and so on.

    Arguments:
        data: A list of elements that form a raster scan
        width: The width of a row of data
        height: The height of the image, the number of rows

    Returns: New data, flipped vertically
    """

    flipped = []
    for row in range(height - 1, -1, -1):
        for col in range(width):
            index = row * width + col
            flipped.append(data[index])
    return flipped


@dataclass
class _CompressPaletteResult:
    data: list[PseudoColorSequence]
    palette: Palette


def _compress_palette(
    animation: list[PseudoColorSequence], palette: Palette
) -> _CompressPaletteResult:
    """Remove all unused palette entires and resize the palette.

    Transprent pixels are untouched
    """
    builder = _CompressPaletteResult([], [])
    old_to_new: dict[PseudoColorPixel, PseudoColorPixel] = {}

    for frame in animation:
        new_data: PseudoColorSequence = []

        for old_data in frame:
            # Don't touch transparent pixels
            if old_data == TRANSPARENT_PIXEL:
                new_data.append(old_data)
                continue

            # Construct new palette
            if old_data not in old_to_new:
                old_to_new[old_data] = len(builder.palette)
                builder.palette.append(palette[old_data])

            # Translate old to new
            new_data.append(old_to_new[old_data])

        builder.data.append(new_data)

    return builder


@dataclass
class PseudoColorAnimation:
    """A sequence of frames that share the same dimensions and palette"""

    width: int
    height: int
    frames: list[PseudoColorSequence]
    palette: Palette

    def compress_palette(self):
        """Mutates self"""
        result = _compress_palette(self.frames, self.palette)
        self.frames = result.data
        self.palette = result.palette

    def flip_vertically(self):
        """Vertically flip all frames of animation. Mutates self"""
        for i in range(len(self.frames)):
            self.frames[i] = _flip_vertically(self.frames[i], self.width, self.height)
