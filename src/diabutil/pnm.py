"""Portable Anymap Format (netpbm) is a useful uncompressed RGB format"""

from pathlib import Path


def save_ppm_p6(width: int, height: int, data: bytes, file: Path):
    with file.open("wb") as fd:
        fd.write(b"P6 ")
        fd.write(str(width).encode())
        fd.write(b" ")
        fd.write(str(height).encode())
        fd.write(b" 255\n")
        fd.write(data)
