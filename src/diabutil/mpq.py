"""A Python ctypes wrapper around StormLib. See https://github.com/ladislav-zezula/StormLib

StormLib must be prebuilt. Then it must be loaded into memory before trying to open an archive.

Example:

    from ctypes import WinDLL
    from pathlib import Path

    from diabutil import mpq

    # Find and load the prebuilt StormLib for windows
    stormlib_path = Path("prebuilts") / "StormLib-Win64" / "StormLib.dll"
    stormlib = WinDll(str(stormlib_path))

    # Open DIABDAT.MPQ with StormLib. The returned type is a context manager which ensure the archive is closed
    with mpq.open_archive(stormlib, "DIABDAT.MPQ") as archive:
        # Read a file from the MPQ into memory
        palette_data = archive.read_file("Levels\\L1Data\\L1_1.PAL")

    # ... do something with palette_data
"""

# TODO WinDLL won't fly in Unix world
from ctypes import pointer, WinDLL
from ctypes.wintypes import BYTE, DWORD, HANDLE
from pathlib import Path


_BASE_PROVIDER_FILE = 0
_SFILE_OPEN_FROM_MPQ = 0
_SFILE_INVALID_SIZE = -1  # 0xFFFFFFFF treated as signed 32 bit


class MpqFile:
    def __init__(self, stormlib: WinDLL, handle: HANDLE):
        self._lib = stormlib
        self._handle = handle

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        del exc_type  # unused, pass exception through
        del exc_value  # unused, pass exception through
        del traceback  # unused, pass exception through
        if not self._lib.SFileCloseFile(self._handle):
            # TODO is raising if an exception caused __exit__ a good idea?
            raise RuntimeError("SFileCloseFile failed")

    def get_size(self) -> int:
        size_lo: int = self._lib.SFileGetFileSize(self._handle, None)
        if size_lo == _SFILE_INVALID_SIZE:
            raise RuntimeError("SFileGetFileSize failed")

        # TODO: read and use size_hi
        return size_lo

    def read(self) -> bytes:
        size = self.get_size()
        contents = (BYTE * size)()
        bytes_read = DWORD()
        if not self._lib.SFileReadFile(
            self._handle, contents, size, pointer(bytes_read), None
        ):
            raise RuntimeError("SFileReadFile failed")

        return bytes(contents)


class MpqArchive:
    """Context manager for reading files in an .MPQ"""

    def __init__(self, stormlib: WinDLL, handle: HANDLE):
        self._lib = stormlib
        self._handle = handle

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        del exc_type  # unused, pass exception through
        del exc_value  # unused, pass exception through
        del traceback  # unused, pass exception through
        if not self._lib.SFileCloseArchive(self._handle):
            # TODO is raising if an exception caused __exit__ a good idea?
            raise RuntimeError("SFileCloseArchive failed")

    def open_file(self, file_name: str) -> MpqFile:
        """Open a file in the MPQ and return it for further processing.

        The returned type is a context manager

        On Windows, file_name must use the backslash sparator
        """
        # TODO check if the file exists or not for better error messages
        file_handle = HANDLE()
        if not self._lib.SFileOpenFileEx(
            self._handle,
            file_name.encode(),
            _SFILE_OPEN_FROM_MPQ,
            pointer(file_handle),
        ):
            raise RuntimeError("SFileOpenFileEx failed")

        return MpqFile(self._lib, file_handle)

    def read_file(self, file_name: bytes) -> bytes:
        """Read a file from an MPQ archive into memory.

        On Windows, file_name must use the backslash sparator
        """
        with self.open_file(file_name) as file:
            return file.read()


def open_archive(stormlib: WinDLL, path: Path) -> MpqArchive:
    """Open an MPQ archive. This is necessary for reading files.

    Arguments:
        stormlib: An open StormLib.DLL
        path: Filesystem path to the MPQ to open

    Returns:
        An open MPQ archive which can be used to read files. This is a context
        manager so should be used via `with`. See the example at the top of the file.
    """
    handle = HANDLE()
    # Priority only matters if we open a patch MPQ or multiple MPQs
    priority = 0
    # TODO: How worried do I need to be that path is LPCWSTR vs LPCSTR?
    if not stormlib.SFileOpenArchive(
        str(path), priority, _BASE_PROVIDER_FILE, pointer(handle)
    ):
        raise RuntimeError("SFileOpenArchive failed")

    return MpqArchive(stormlib, handle)
