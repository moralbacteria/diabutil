"""CEL is a container for 8-bit animation data stored with RLE.

The RLE compresses transparent runs, significantly reducing filesize compared to uncompressed list of 8-bit pixels

There's many types of graphics stored in CEL that it's basically useless as a format:

- Animations like PentSpin.cel
- Logical group of graphics with differing dimensions like objcurs.cel
- A bunch of animations crammed into one file, like all plrgfx. This case is special because there's an additional header which contains offsets to each animation sequence. The data that the offset points to can be treated as one of the above CELs.
- Dungeon tile CELs which have an implicit shape (defined elsewhere)

CELs also lack important metadata:

- Whether the frames have a skip header
- Image dimensions
- Which palette to use
- Whether it is a CEL or a collection of CELs
"""

from . import PseudoColorAnimation
from .pal import Palette
from .types import (
    TRANSPARENT_PIXEL,
    CelRleSequence,
    PseudoColorSequence,
)
from .util import (
    BufferedIterator,
    pair_sliding_window,
    find_groups,
    find_frames,
)

from dataclasses import dataclass
from enum import Enum


class _RleOpCode(Enum):
    TRANSPARENCY = 1
    COPY = 2


@dataclass
class _Instruction:
    opcode: _RleOpCode
    length: int

    @classmethod
    def decode(cls, data: int):
        if data < 0:
            return cls(_RleOpCode.TRANSPARENCY, -data)
        return cls(_RleOpCode.COPY, data)


class _RleDecoder:
    """Uses the bytes iterator to produce runs"""

    def __init__(self, iterator: BufferedIterator):
        self._iterator = iterator

    def next_run(self) -> PseudoColorSequence:
        instruction = _Instruction.decode(self._iterator.next_int8())
        match instruction.opcode:
            case _RleOpCode.TRANSPARENCY:
                return [TRANSPARENT_PIXEL] * instruction.length
            case _RleOpCode.COPY:
                return [x for x in self._iterator.next_bytes(instruction.length)]
        raise RuntimeError(f"Invalid instruction: {instruction.opcode}")


# TODO: prefer generator?
def rle_decode(iterator: BufferedIterator, end: int) -> PseudoColorSequence:
    """Decode .CEL RLE data into an uncompressed lsit of palette indices.

    Arguments:
        iterator: Raw RLE data from the .CEL file (without skip header)
        end: Offset at which to stop

    Returns:
        Uncompressed image data. The is laid out bottom-to-top so most 2D
        frameworks will want the image vertically flipped. This data does not
        include image dimensions. Transparent pixels are -1
    """
    builder: PseudoColorSequence = []
    decoder = _RleDecoder(iterator)
    while iterator.tell() < end:
        builder += decoder.next_run()
    return builder


def rle_decode_sequence(data: CelRleSequence) -> PseudoColorSequence:
    """Like rle_decode but you give bytes instead of an iterator"""
    return rle_decode(BufferedIterator(data), len(data))


def find_frame_data(data: bytes, has_skip_header: bool = False) -> list[CelRleSequence]:
    """Find all the frames, but return them raw. Don't bother trying to RLE
    decode them. This is useful for tile rendering; not everything is RLE
    encoded in that case!
    """
    frame_data: list[CelRleSequence] = []

    iterator = BufferedIterator(data)
    frame_offsets = find_frames(iterator)
    for frame_begin_offset, frame_end_offset in pair_sliding_window(frame_offsets):
        if has_skip_header:
            frame_begin_offset += 10
        iterator.seek(frame_begin_offset)
        frame_data.append(iterator.next_bytes(frame_end_offset - frame_begin_offset))

    return frame_data


def decode_frame_data(
    data: bytes, has_skip_header: bool = False
) -> list[PseudoColorSequence]:
    return [
        rle_decode_sequence(encoded_frame)
        for encoded_frame in find_frame_data(data, has_skip_header)
    ]


def deserialize(
    data: bytes,
    palette: Palette,
    width: int,
    has_skip_header: bool = False,
) -> PseudoColorAnimation:
    """Load, parse, and decode a CEL into memory.

    Arguments:
        path: The path on disk from which to load the .CEL
        palette: A palette already loaded into memory
        width: The width of a frame
        has_skip_header: Whether the frames have a skip header

    Returns:
        A pseudocolor sequence of frames with metadata ready to be manipulated.

    Example:
        from pathlib import Path
        from diabutil import cel, gif, pal

        # Read and decode PentSpin.cel into memory
        PentSpin = cel.deserialize(
            Path('PentSpin.cel').read_bytes(),
            pal.load_palette(Path('l1.pal')),
            width=48)

        # Save the animation as gif
        PentSpin.compress_palette()
        transparency = gif.resolve_transparency(PentSpin)
        gif.save_gif(PentSpin, transparency, gif.DIABLO_DELAY, Path('PentSpin.gif'))
    """
    decoded_frames = decode_frame_data(data, has_skip_header)
    height = int(len(decoded_frames[0]) / width)
    animation = PseudoColorAnimation(width, height, decoded_frames, palette)
    animation.flip_vertically()
    return animation


def deserialize_with_groups(
    data: bytes,
    palette: Palette,
    width: int,
    num_groups: int,
    has_skip_header: bool = False,
) -> list[PseudoColorAnimation]:
    """Load, parse, and decode a CEL into memory.

    Arguments:
        path: The path on disk from which to load the .CEL
        palette: A palette already loaded into memory
        width: The width of a frame
        has_skip_header: Whether the frames have a skip header

    Returns:
        A list of animations, one per group (direction).

    Example:
        from diabutil import cel, gif, pal
        from pathlib import Path

        # Read and decode whtat.cel into memory
        whtat = cel.deserialize_with_groups(
            Path('whtat.cel').read_bytes(),
            pal.load_palette(Path('l1.pal')),
            width=128,
            num_groups=8,
            has_skip_header=True)

        # Save the first direction (south) as gif
        whtat[0].compress_palette()
        transparency = gif.resolve_transparency(whtat[0])
        gif.save_gif(whtat[0], transparency, gif.DIABLO_DELAY, Path('whtat_0.gif'))
    """
    builder = []

    iterator = BufferedIterator(data)
    group_offsets = find_groups(iterator, num_groups)
    # group_header_size = iterator.tell()
    for group_offset in group_offsets:
        decoded_frames = []

        iterator.seek(group_offset)
        frame_offsets = [x + group_offset for x in find_frames(iterator)]
        for frame_begin_offset, frame_end_offset in pair_sliding_window(frame_offsets):
            if has_skip_header:
                frame_begin_offset += 10
            iterator.seek(frame_begin_offset)
            decoded_frames.append(rle_decode(iterator, frame_end_offset))

        height = int(len(decoded_frames[0]) / width)
        animation = PseudoColorAnimation(width, height, decoded_frames, palette)
        animation.flip_vertically()

        builder.append(animation)

    return builder
