"""GIF serializer for animations (or static images) with transparency and 126
colors. Images are stored as 8-bit uncompressed LZW by emitting the stop code
every 126 pixels. This made the implementation simpler.

https://www.w3.org/Graphics/GIF/spec-gif89a.txt
"""

from . import pal, PseudoColorAnimation, TRANSPARENT_PIXEL
from .types import Color, Palette, PseudoColorSequence, COLOR_ENCODED_SIZE

from pathlib import Path
from typing import Any


_ONE_SECOND_IN_HUNDREDTHS = 100
_DIABLO_FPS = 20

# Ideal GIF frame delay for animations that run with 0 AnimDelay. Multiply by
# AnimDelay + 1 to emulate higher delays
#
# For plrgfx the AnimDelay is typically set by the Start*() player functions
DIABLO_DELAY = int(_ONE_SECOND_IN_HUNDREDTHS / _DIABLO_FPS)


_7BIT_MAX_VALUE = 127
_GLOBAL_COLOR_TABLE_SIZE = _7BIT_MAX_VALUE + 1
# To keep an 8-bit code, we need to prevent generating a 9-bit code. This
# happens when we try to add a new symbol to a full dictionary. 8-bit codes
# use 0x00-0x7F as palette, 0x80 as clear, and 0x81 as stop. That leaves
# 0x82-0xFF for new entries (126 values). Thus, we must emit a CLEAR code every
# 126 values.
#
# This allows me to store uncompressed image data, which is easier to implement
_LZW_DICTIONARY_MAX_NEW_ENTRIES = 126
_LZW_8BIT_STOP_CODE = 0x81
_LZW_8BIT_CLEAR_CODE = 0x80
_GIF_MAX_SUB_BLOCK_SIZE = 255
_BRIGHT_PINK = Color(255, 0, 255)
_TRANSPARENT_COLOR = _BRIGHT_PINK


def _replace_all(lst: list[Any], old: Any, new: Any):
    return [new if x == old else x for x in lst]


def resolve_transparency(animation: PseudoColorAnimation) -> int:
    """Create a transparent palette index, make the animation use that.

    Returns: The palette index of the transparent color
    """
    if len(animation.palette) >= _GLOBAL_COLOR_TABLE_SIZE - 1:
        raise RuntimeError("No space in palette for transparency!")

    # Add the transparent color
    transparent = len(animation.palette)
    animation.palette.append(_TRANSPARENT_COLOR)

    for i in range(len(animation.frames)):
        animation.frames[i] = _replace_all(
            animation.frames[i], TRANSPARENT_PIXEL, transparent
        )

    return transparent


def _as_byte(i: int):
    """GIF-spec Byte is 1 byte, unsigned, little endian"""
    return i.to_bytes(1, "little")


def _as_unsigned(i: int):
    """GIF-spec Unsigned is 2 bytes little endian"""
    return i.to_bytes(2, "little")


def _write_header(fd):
    fd.write(b"GIF")  # Signature
    fd.write(b"89a")  # Version


def _write_logical_screen_descriptor(fd, width: int, height: int, background: int):
    fd.write(_as_unsigned(width))
    fd.write(_as_unsigned(height))
    # image has global color table
    # original image had 7+1 bits per primary color
    # the global color table is not ordered
    # the global color table is 3*2^(6+1) large
    packed_fields = 0b11110110  # TODO: make these params
    fd.write(_as_byte(packed_fields))
    fd.write(_as_byte(background))  # Background color
    fd.write(b"\x00")  # Pixel aspect ratio


def _write_global_color_table(fd, palette: Palette):
    assert len(palette) <= _GLOBAL_COLOR_TABLE_SIZE
    pal_bytes = pal.encode_palette(palette)
    missing = (_GLOBAL_COLOR_TABLE_SIZE * COLOR_ENCODED_SIZE) - len(pal_bytes)
    if missing > 0:
        pal_bytes += b"\x00" * missing
    fd.write(pal_bytes)


def _write_netscape_application_block(fd):
    # This allows animation and looping.
    # From wikipedia: https://en.wikipedia.org/wiki/GIF#Animated_GIF
    fd.write(b"!")  # Extension introducer
    fd.write(b"\xFF")  # Extension label, must be 0xFF
    fd.write(b"\x0b")  # Block size, must be 11
    fd.write(b"NETSCAPE")  # Application Identifier
    fd.write(b"2.0")  # Authentication code
    # Application Data
    fd.write(b"\x03")  # Sub-block size
    fd.write(b"\x01")  # Always 1 for NETSCAPE block
    fd.write(b"\xFF\xFF")  # Unsighed number of reptitions TODO make this 0?
    fd.write(b"\x00")  # Block Terminator


def _write_graphic_control_extension(fd, delay: int, transparency: int):
    fd.write(b"!")  # Extension introducer
    fd.write(b"\xF9")  # Label, must be 0xF9
    fd.write(b"\x04")  # Block size = 4
    # disposal method 2 (restore background)
    # no user input
    # yes there is transparency
    packed_fields = 0b00001001  # TODO: make these params
    fd.write(_as_byte(packed_fields))
    fd.write(_as_unsigned(delay))
    fd.write(_as_byte(transparency))  # transparency index
    fd.write(b"\x00")  # Block Terminator


def _write_image_descriptor(fd, width: int, height: int):
    fd.write(b",")  # Image Separator
    fd.write(b"\x00\x00")  # Image Left Position (0 means left-most edge)
    fd.write(b"\x00\x00")  # Image Top Position (0 means top-most edge)
    fd.write(_as_unsigned(width))  # Image width
    fd.write(_as_unsigned(height))  # Image Height
    # TODO: make packed fields into params
    fd.write(b"\x00")  # Packed fields (no local color table, not interlaced)


def _write_table_based_image_data(fd, lzw_data: bytes):
    fd.write(b"\x07")  # Minimum LZW code size is 7
    # Carve into sub-blocks and write out
    for sub_block_start in range(0, len(lzw_data), _GIF_MAX_SUB_BLOCK_SIZE):
        remaining = len(lzw_data) - sub_block_start
        sub_block_size = min(remaining, _GIF_MAX_SUB_BLOCK_SIZE)
        fd.write(_as_byte(sub_block_size))
        fd.write(lzw_data[sub_block_start : sub_block_start + sub_block_size])
    fd.write(b"\x00")  # Block terminator


def _write_trailer(fd):
    fd.write(b";")  # End of data stream


def _lzw_encode(frame: PseudoColorSequence) -> bytes:
    # Add clear code to hack around LZW dict getting too big and generating
    # 9-bit codes. This allows me to store uncompressed image data,
    # which is easier to implement
    lzw_data = []
    # TODO: simplify with ranges/slices
    for i in range(0, len(frame), _LZW_DICTIONARY_MAX_NEW_ENTRIES):
        lzw_data.append(_LZW_8BIT_CLEAR_CODE)
        lzw_data += frame[i : i + _LZW_DICTIONARY_MAX_NEW_ENTRIES]
    lzw_data.append(_LZW_8BIT_STOP_CODE)
    return bytes(lzw_data)


def save_gif(animation: PseudoColorAnimation, transparent: int, delay: int, path: Path):
    # TODO: We only support 7-bit color (8-bit codes)
    if len(animation.palette) >= _GLOBAL_COLOR_TABLE_SIZE:
        raise RuntimeError("Image has too many colors")

    # TODO support non-transparent gifs
    if transparent < 0 or transparent >= _GLOBAL_COLOR_TABLE_SIZE:
        raise RuntimeError("transparent index is invalid")

    # Support static GIFs (no animation) by overriding delay
    if len(animation.frames) == 1:
        delay = 0

    with path.open("wb") as fd:
        _write_header(fd)
        _write_logical_screen_descriptor(
            fd, animation.width, animation.height, transparent
        )
        _write_global_color_table(fd, animation.palette)
        _write_netscape_application_block(fd)

        for frame in animation.frames:
            _write_graphic_control_extension(fd, delay, transparent)
            _write_image_descriptor(fd, animation.width, animation.height)
            lzw_data = _lzw_encode(frame)
            _write_table_based_image_data(fd, lzw_data)

        _write_trailer(fd)
