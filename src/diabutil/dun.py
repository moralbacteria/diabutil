from .util import BufferedIterator

from dataclasses import dataclass


@dataclass
class DunFile:
    width: int
    height: int
    tiles: list[int]


def decode_dun(data: bytes) -> DunFile:
    iterator = BufferedIterator(data)
    width = iterator.next_uint16()
    height = iterator.next_uint16()
    tiles = []
    for _ in range(width * height):
        tiles.append(iterator.next_uint8())
        iterator.next_uint8()  # skip a byte
    return DunFile(width, height, tiles)
