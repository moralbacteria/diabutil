# diabutil

Python scripts for reading Diablo file formats:

- .CEL
- .CL2
- .PAL
- .DUN
- .TIL
- .MIN
- .MPQ 

as well as support for saving to the following image formats:

- GIF with animation and transparency (only 128 colors; uncompressed)
- PPM

## Running samples

```
pipenv install .
cd samples
pipenv run python <sample>
```

Some samples to checkout:

- visualize.py (requires demo MPQ)
- whtat.py (requires Beta MPQ)
- BloodFnt.py (requires demo MPQ)

All samples expect an MPQ to be in the samples directory.

## samples/visualize.py

This sample script demonstrates how to use diabutil to render the files of the
Pre-release Demo. This includes:

- Palettes saved as PPM
- Animated CELs saved as GIF (e.g. PlrGFX)
- Rendering CELs like Objcurs.CEL as individual GIF frames
- Rendering CELs as sprite sheets
- Non-transparent static CELs saved as PPM (e.g. Gendata/*)
- DUN rendering using .MIN, .TIL, L1.CEL, L1S.CEL, etc.

Here's an example that demonstrates how to open an MPQ, read a bunch of files, render a CEL animation (`BloodFnt.CEL`), and save it as a GIF:

```py
from ctypes import WinDLL
from pathlib import Path

from diabutil import cel, gif, mpq, pal

#
# First we need to load and decode the CEL (and associated files) from the MPQ
#

# StormLib is used to extract files from the MPQ. We provide a Python ctypes
# wrapper. You just have to provide it the shared library. In this case, we
# use the prebuilt that we checked in to the repo.
stormlib_path = Path('prebuilts') / 'StormLib-Win64' / 'StormLib.dll'
stormlib = WinDLL(str(stormlib_path))
with mpq.open_archive(stormlib, 'DIABDAT.MPQ') as archive:
    # Load the .PAL file, required to turn pseudocolor graphics into RGB
    palette = pal.decode_pal(archive.read_file(b'Levels\\L1Data\\L1_1.PAL'))

    # Load the .CEL. The CEL format doesn't specify certain things like which
    # palette to use, what the width is, and whether there is a 10-byte skip
    # header (only useful during in-game rendering so we ignore it).
    #
    # The result is a list of uncompressed pseudocolor frames wrapped in a
    # PseudoColorAnimation
    BloodFnt_anim = cel.deserialize(
        archive.read_file(b'Objects\\BloodFnt.CEL'),
        palette,
        width=96,
        has_skip_header=True)

    # There's a bunch of CEL decoding functions that are aimed at different CELs
    # e.g. use deserialize_with_groups for PlrGFX, find_frame_data for L1.CEL,
    # etc. We have to provide all these functions since each CEL is a special
    # snowflake...

#
# Second, we can save the CEL animation as GIF
#

# Our GIF serializer is limited to 128 colors so compress the palette. Not
# usually a problem (due to the design of the palettes) but sometimes there are
# edge cases
BloodFnt_anim.compress_palette()

# So far, transparency has been represented by the sentinel -1. This can't be
# saved to GIF directly. Instead, we dedicate a palette entry to it then store
# the index to that entry in the GIF as transparency. Then, any compliant GIF
# renderer will treat that color as transparent
transparency = gif.resolve_transparency(BloodFnt_anim)

# Serialize to disk as GIF. We provide the animation (which has the actual image
# data), the transparency palette index from resolve_transparency (so it gets
# included in the GIF), the delay in milliseconds between frames of animation,
# and the output path.
#
# Diablo operates at 20 FPS which we present as gif.DIABLO_DELAY. However, some
# animations have AnimDelay which makes them slower. This is encoded into the
# DIABLO.EXE binary and can only be found via close inspection of the
# disassembly... Another reason the CEL format sucks ;)
gif.save_gif(
    BloodFnt_anim, transparency, gif.DIABLO_DELAY*2, Path('BloodFnt.gif'))
```

## Future Work

My list of TODOs (in no order) includes:

- Support for 256 color gifs
- Support for compressed GIFs
- Document all the things
- Change MPQ API to take a str (not bytes)
- StormLib prebuilts for non-Windows platforms
- .AMP and .SOL
- TownData rendering
- Other missing files from demo MPQ in visualize.py (PlrGFX, towners, etc)
- Get dungeon rendering into src/ somehow
- Create a package and upload to pypi
- Speed improvements (esp. dungeon rendering); consider writing some parts in C?
- Dungeon rendering objects and monsters
- Python MPQ loader (to replace stormlib)
- Demo VS final game L1.CEL rendering
- Conan stormlib?
- Monster variants (TRN)
- Unique monsters (TRN)
- Refactoring - unify parts of CEL/CL2 loading
- Refactoring - API consistency and nomenclature - parse vs decode; PseudoColorSequence vs Image vs PseudoColorAnimation
- mypy type annotations
- unit tests

## Developing

```
pipenv install -d .
```

Format before commit

```
pipenv run black .
pipenv run mypy src
```

NB default line length for black is 88